# Virtual Wallet
### Transfer money quick, safe, and free

## Project Description
**Virtual Wallet** is a web application that enables you to consistently manage your budget. Every user can send and receive money (user to user) and put money in his **Virtual Wallet** (bank to app). **Virtual Wallet** has a core set of requirements that are absolute <span style="color:red">must</span> and a variety of optional features.
## Functional Requirements
### Entities
- Each **user** must have a username, password, email, phone number, credit/debit card and a photo.
    - Username <span style="color:red">must</span> be unique and between 2 and 20 symbols.
    - Password <span style="color:red">must</span> be at least 8 symbols and should contain capital letter, digit and special symbol (+, -, *, &, ^, …)
    - Email <span style="color:red">must</span> be valid email and unique in the system.
    - Phone number <span style="color:red">must</span> be 10 digits and unique in the system.
- **Credit/debit card** <span style="color:red">must</span> have a number, expiration date, card holder and a check number
    - Card number <span style="color:red">must</span> be unique and with 16 digits.
    - Card holder <span style="color:red">must</span> be between 2 and 30 symbols.
    - Check number <span style="color:red">must</span> be 3 digits.

### Public Part
The public part <span style="color:red">must</span> be accessible without authentication i.e., for anonymous user. 

Anonymous userEntities <span style="color:red">must</span> be able to register and login.

Anonymous userEntities <span style="color:red">must</span> see detailed information about Virtual Wallet and its features.

### Private part
Accessible only if the user is authenticated.

Users <span style="color:red">must</span> be able to login/logout, update their profile, manage their credit/debit card, make transfers to other userEntities, and view the history of their transfers.

Users <span style="color:red">must</span> be able to view and edit their profile information, except their username, which is selected on registration and cannot be changed afterwards. The required fields for registration are username, email, and phone number.

Each user <span style="color:red">must</span> be able to register one credit or debit card, which is used to transfer money into their **Virtual Wallet**.

*Note: The transfer is done by a separate ***dummy*** REST API. It must provide a single endpoint for money withdraw from the credit/debit card. It confirms transfers on random basis (it is dummy, right).*

*Note: ***DO NOT*** use actual credit card information!*

Users <span style="color:red">must</span> be able to transfer money to other userEntities by entering another user's phone number, username or email and desired amount to be transferred. Users can search by phone number, username, or email to select the recipient user for the transfer, but when viewing recipient userEntities only username must be displayed.

Each transfer <span style="color:red">must</span> go through confirmation step which displays the transfer details and allows either confirming it or editing it.

Users <span style="color:red">must</span> be able to view a list of their transactions filtered by period, recipient, and direction (incoming or outgoing) and sort them by amount and date. Transaction list <span style="color:yellow">should</span> support pagination.

### Administrative part
  
Accessible to users with administrative privileges.

Admin users <span style="color:red">must</span> be able to see list of all userEntities and search them by phone number, username or email and block or unblock them. User list <span style="color:yellow">should</span> support pagination. A blocked user must be able to do everything as normal user, except to make transactions.

Admin users <span style="color:red">must</span> be able to view a list of all user transactions filtered by period, sender, recipient, and direction (incoming or outgoing) and sort them by amount and date. Transaction list <span style="color:yellow">should</span> support pagination.

### Optional features

**Email Verification** – In order for the registration to be completed, the user must verify their email by clicking on a link send to their email by the application. Before verifying their email, userEntities cannot make transactions.

**Large Transaction Verification** – In order to complete transactions over a certain amount (up to you), the user is prompted to enter a verification code, sent to their email. The code should be unique for the transaction and have expiration time.

**Refer a Friend** – A user can enter email of people, not yet registered for the application, and invite them to register. The application sends to that email a registration link. If a registration from that email is completed and verified, both userEntities receive a certain amount (up to you) in their virtual wallet. Invitations have an expiration time, and a user can take advantage of that feature a limited number of times (up to you).

**Identity Verification** – In order for the user registration to be completed, the user must submit a photo of their id card and a selfie. Users with administrator rights should have a page where they can view all userEntities waiting for verification, review the photos they submitted and approve or reject them. Before being approved, userEntities cannot make transactions.
  
*Note: ***DO NOT*** upload actual photos of id cards!*

**Joint Virtual Wallets** – User can create joint virtual wallets. They function as the regular wallets; however, multiple userEntities can use them. The original creator of the wallet has an administration panel for the wallet, where they can grant or revoke other user’s access to spend or add money to the wallet. When making a transaction or adding money to wallet, userEntities with access to multiple wallets must select, which one to use. The Transaction History Page show which wallet was used for the transaction.

**Recurring Transactions** – Users can set up recurring transactions. When creating a transaction, the user has the option to select an interval of time on which the transaction is repeated automatically. Users have a page, where they can view all their recurring transactions and cancel them. Users must be notified if their transactions failed for some reason.

**Contacts List** – In addition to searching through all the application userEntities, a user can create a contacts list. A user can add another user to their contacts list either from the transaction profile search or from the Transactions History Page. On the Create Transaction Page the user must select if the transaction is from the contacts list or from the application userEntities list. The user has a contact list administration page, where they can remove userEntities from the list.

**Multiple Virtual Wallets** – A user can create more than one wallet. When creating a transaction, the user is prompted to select, which wallet to use. The Transaction History Page show which wallet was used for the transaction. The user can set a default wallet, which is preselected when creating transactions.

**Multiple Cards** – A user can register multiple credit and or debit cards, from which to add funds to their accounts. When adding funds to their wallet, the user is prompted to select from which bank account to do so.

**Overdraft** – A user can enable overdrafts on their wallets. If overdraft is enabled, the user’s Virtual Wallet balance can go below 0, up to a certain amount (up to you) when making transactions. At 00:00 on the first of each month wallets with enabled overdraft are charged interest. How you calculate the interest is up to you. If a user’s total Virtual Wallet balance doesn’t go above 0 for a number (up to you) of consecutive months, the user’s account is blocked until enough money is added to it to cover the negative balance. Admin userEntities have a page where they can change modifiers for the interest rate for future Savings Wallets (already existing ones should not be affected).

**Savings Wallet** – User can create Virtual Savings Wallets. The user choses an amount and a duration for the wallet and is shown the interest rate they are going to receive. The user can review the Savings Wallet creation and confirm it or go back and edit the details. The interest rate should vary based on the duration and amount saved (how you calculate it is up to you). Once the selected duration for the account passes, the saved amount plus the interest is automatically added to the wallet, from which they were taken from. The user has a page where they can view their Savings Wallets.

**Spending Categories** – When creating a transaction, a user can select a category for the transfer (Rent, Utilities, Eating out etc.). The user has a page to manage their categories. They can add, edit, or delete them. The user also has a reports page where they can select a period and see a breakdown of their spending by category.

**Currency Support** – When creating their Virtual Wallet userEntities, can choose a currency for it. The currency exchange rate is shown on transactions between different currencies. The exchange rate and supported currencies are managed by admin userEntities.

**Easter eggs** – Creativity is always welcome and appreciated. Find a way to add something fun and/or interesting, maybe an Easter egg or two to you project to add some variety. 

### REST API
To provide other developers with your service, you need to develop a REST API. It should leverage HTTP as a transport protocol and clear text JSON for the request and response payloads.

A great API is nothing without a great documentation. The documentation holds the information that is required to successfully consume and integrate with an API. You <span style="color:red">must</span> use Swagger to document yours.

The REST API provides the following capabilities:
1. Users
   - CRUD Operations <span style="color:red">(must)</span>
   - Add/view/update/delete credit/debit card <span style="color:red">(must)</span>
   - Block/unblock user <span style="color:red">(must)</span>
   - Search by username, email, or phone <span style="color:red">(must)</span>

2. Transactions
   - Add money to wallet <span style="color:red">(must)</span>
   - Make transaction <span style="color:red">(must)</span>
   - List transactions <span style="color:red">(must)</span>
   - Filter by date, sender, recipient, and direction (in/out) <span style="color:red">(must)</span>
   - Sort by date or amount <span style="color:red">(must)</span>
   
3. Transfers
   - Withdraw <span style="color:red">(must)</span>
      
## Technical Requirements
### General
- Follow [OOP](https://en.wikipedia.org/wiki/Object-oriented_programming) principles when coding
- Follow [KISS](https://en.wikipedia.org/wiki/KISS_principle), [SOLID](https://en.wikipedia.org/wiki/SOLID), [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) principles when coding
- Follow REST API design [best practices](https://blog.florimondmanca.com/restful-api-design-13-best-practices-to-make-your-users-happy) when designing the REST API (see Appendix)
- Use tiered project structure (separate the application in layers)
- The service layer (i.e., "business" functionality) <span style="color:red">must</span> have at least 80% unit test code coverage
- Follow [BDD](https://en.wikipedia.org/wiki/Behavior-driven_development) when writing unit tests
- You should implement proper exception handling and propagation
- Try to think ahead. When developing something, think – “How hard would it be to change/modify this later?”

### Database
The data of the application <span style="color:red">must</span> be stored in a relational database. You need to identify the core domain objects and model their relationships accordingly. Database structure should avoid data duplication and empty data (normalize your database).

Your repository <span style="color:red">must</span> include two scripts – one to create the database and one to fill it with data.

### Git

Commits in the GitLab repository should give a good overview of how the project was developed, which features were created first and the people who contributed. Contributions from all team members <span style="color:red">must</span> be evident through the git commit history! The repository <span style="color:red">must</span> contain the complete application source code and any scripts (database scripts, for example).

Provide a link to a GitLab repository with the following information in the README.md file:

- Project description
- Link to the Swagger documentation <span style="color:red">(must)</span>
- Link to the hosted project (if hosted online)
- Instructions how to setup and run the project locally 
- Images of the database relations <span style="color:red">(must)</span>

### Optional Requirements

Besides all requirements marked as <span style="color:yellow">should</span> and <span style="color:green">could</span>, here are some more *optional* requirements:

- Use a branching while working with Git.
- Integrate your app with a Continuous Integration server (e.g., GitLab’s own) and configure your unit tests to run on each commit to the master branch.
- Host your application’s backend in a public hosting provider of your choice (e.g., AWS, Azure, Heroku).

## Teamwork Guidelines

Please see the Teamwork Guidelines document. 

## Appendix
- [Guidelines for designing good REST API](https://blog.florimondmanca.com/restful-api-design-13-best-practices-to-make-your-users-happy)
- [Guidelines for URL encoding](https://www.talisman.org/~erlkonig/misc/lunatech%5Ewhat-every-webdev-must-know-about-url-encoding/)
- [Always prefer constructor injection](https://www.vojtechruzicka.com/field-dependency-injection-considered-harmful/)
- [Git commits - an effective style guide](https://dev.to/pavlosisaris/git-commits-an-effective-style-guide-2kkn)
- [How to Write a Git Commit Message](https://cbea.ms/git-commit/)

## Legend
- <span style="color:red">Must</span> – Implement these first.
- <span style="color:yellow">Should</span> – if you have time left, try to implement these.
- <span style="color:green">Could</span> – only if you are ready with everything else give these a go.
