package com.example.virtualwallet.exceptions;

public class InvalidRecaphaException extends RuntimeException{

    public InvalidRecaphaException(String message) {
        super(message);
    }
}
