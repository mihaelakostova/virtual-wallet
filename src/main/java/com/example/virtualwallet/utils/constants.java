package com.example.virtualwallet.utils;

public class constants {


    public static final String ONLY_USER_WHO_ARE_IN_WALLET_CAN_BE_REMOVE = "Only user who are in wallet can be removed";

    public static final String ONLY_ADMIN_OR_CARD_OWNER_CAN_DELETE = "Only ard owner have access";
    public static final String ONLY_ADMIN_OR_WALLET_OWNER_CAN_DELETE = "Only Wallet owner have access";

    public static final String G_RECAPTCHA_RESPONSE = "g-recaptcha-response";

    public static final String RECAPTCHA_SECRET_ATT = "recaptcha.secret";
    public static final String RECAPTCHA_API = "https://www.google.com/recaptcha/api/siteverify";
    public static final String QUERY_SECRET = "?secret=";
    public static final String AND_RESPONSE = "&response=";
}
