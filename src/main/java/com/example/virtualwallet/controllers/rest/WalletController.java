package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.emus.CurrencyType;
import com.example.virtualwallet.models.filters.WalletFilterOptions;
import com.example.virtualwallet.services.contracts.CurrencyExchangeService;
import com.example.virtualwallet.services.contracts.UserService;
import com.example.virtualwallet.services.contracts.WalletService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/wallets")
@Api(tags = "Wallet REST")
public class WalletController {

    private final WalletService walletService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final CurrencyExchangeService currencyExchangeService;

    @Autowired
    public WalletController(WalletService walletService, UserService userService, AuthenticationHelper authenticationHelper, CurrencyExchangeService currencyExchangeService) {
        this.walletService = walletService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.currencyExchangeService = currencyExchangeService;
    }

    @ApiOperation("Get filter wallets")
    @GetMapping
    public List<Wallet> get(
            @RequestParam(required = false) String number,
            @RequestParam(required = false) Double minBalance,
            @RequestParam(required = false) Double maxBalance,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String orderBy) {

        WalletFilterOptions filterOptions = new WalletFilterOptions(number, minBalance, maxBalance, sortBy, orderBy);
        return walletService.getAll(filterOptions);
    }

    @ApiOperation("Get wallets")
    @GetMapping("/all")
    public List<Wallet> getAll() {
        return walletService.getAll();
    }

    @ApiOperation("Get wallet by id")
    @GetMapping("/{id}")
    public Wallet getById(@PathVariable int id) {
        try {
            return walletService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Add user to wallet")
    @GetMapping("/{id}/add/{username}")
    public void addUser(@RequestHeader HttpHeaders headers, @PathVariable int id, @PathVariable String username) {
        try {
            User logUser = authenticationHelper.tryGetUser(headers);
            User user = userService.getByUsername(username);
            Wallet wallet = walletService.getById(id);
            walletService.addUserToWallet(logUser, user, wallet);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation("Remove user from wallet")
    @GetMapping("/{id}/remove/{username}")
    public void removeUser(@RequestHeader HttpHeaders headers, @PathVariable int id, @PathVariable String username) {
        try {
            User logUser = authenticationHelper.tryGetUser(headers);
            User user = userService.getByUsername(username);
            Wallet wallet = walletService.getById(id);
            walletService.removeUserFromWallet(logUser, user, wallet);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation("Create wallet")
    @PostMapping
    public void create(@RequestHeader HttpHeaders headers) {
        User user = authenticationHelper.tryGetUser(headers);
        walletService.create(user);
    }

    @ApiOperation("Delete wallet")
    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            walletService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation("Change status wallet")
    @PutMapping("/{id}/status")
    public void updateStatus(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User userAuthorization = authenticationHelper.tryGetUser(headers);
            Wallet wallet = walletService.getById(id);
            walletService.changeBlockStatus(wallet, userAuthorization);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Get balance by currency of wallet")
    @GetMapping("/{id}/balance/{currency}")
    public String getWalletBalanceInCurrency(@PathVariable int id, @PathVariable CurrencyType currency) {
        try {
            Wallet wallet = walletService.getById(id);
            double balance = wallet.getBalance();

            double convertedBalance = currencyExchangeService.exchange(wallet.getCurrency(), currency, balance);

            return "Wallet balance in " + currency + ": " + convertedBalance;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
