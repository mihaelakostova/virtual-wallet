package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.CardMapper;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.CardDto;
import com.example.virtualwallet.models.filters.CardFilterOptions;
import com.example.virtualwallet.services.contracts.CardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("api/cards")
@Api(tags = "Card REST")
public class CardController {

    private final AuthenticationHelper authenticationHelper;
    private final CardService cardService;
    private final CardMapper cardMapper;

    @Autowired
    public CardController(AuthenticationHelper authenticationHelper, CardService cardService, CardMapper cardMapper) {
        this.authenticationHelper = authenticationHelper;
        this.cardService = cardService;
        this.cardMapper = cardMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("loggedInUser")
    public User getCurrentUser(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            return authenticationHelper.tryGetUser(httpSession);
        }
        return null;
    }

    @ApiOperation("Get my cards")
    @GetMapping("/my")
    public List<Card> getByUser(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return cardService.getByUser(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation("Get card by id")
    @GetMapping("/{id}")
    public CardDto getById(@PathVariable int id) {
        try {
            Card card = cardService.getById(id);
            return cardMapper.toDto(card);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Add card")
    @PostMapping
    public CardDto create(@RequestHeader HttpHeaders headers, @Valid @RequestBody CardDto cardDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Card card = cardMapper.fromDto(cardDto);
            cardService.create(card, user);
            return cardMapper.toDto(card);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation("Delete card")
    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            cardService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation("Change status card")
    @PutMapping("/{id}/status")
    public void updateStatus(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User userAuthorization = authenticationHelper.tryGetUser(headers);
            Card card = cardService.getById(id);
            cardService.changeBlockStatus(card, userAuthorization);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
