package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.TransferMapper;
import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.TransferDto;
import com.example.virtualwallet.models.dtos.WithdrawDto;
import com.example.virtualwallet.services.contracts.TransferService;
import com.example.virtualwallet.services.contracts.WalletService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/transfers")
@Api(tags = "Transfer REST")
public class TransferController {
    private final AuthenticationHelper authenticationHelper;
    private final TransferService transferService;
    private final TransferMapper transferMapper;
    private final WebClient.Builder webClientBuilder;
    private final WalletService walletService;

    public TransferController(AuthenticationHelper authenticationHelper, TransferService transferService, TransferMapper transferMapper, WebClient.Builder webClientBuilder, WalletService walletService) {
        this.authenticationHelper = authenticationHelper;
        this.transferService = transferService;
        this.transferMapper = transferMapper;
        this.webClientBuilder = webClientBuilder;
        this.walletService = walletService;
    }

    @ApiOperation("Get transfer")
    @GetMapping
    public List<TransferDto> get() {
        List<Transfer> transfers = transferService.getAll();
        return transferMapper.toDto(transfers);
    }

    @ApiOperation("Get transfer by ID")
    @GetMapping("/{id}")
    public TransferDto getById(@PathVariable int id) {
        try {
            Transfer transfer = transferService.getById(id);
            return transferMapper.toDto(transfer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Create transfer")
    @PostMapping
    public TransferDto create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TransferDto transferDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Transfer transfer = transferMapper.fromDto(transferDto);
            transferService.create(transfer, user);
            return transferMapper.toDto(transfer);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation("Withdraw")
    @GetMapping("/{id}/withdraw")
    public Mono<ResponseEntity<String>> withdraw(@PathVariable int id,
                                                 @RequestHeader HttpHeaders headers,
                                                 @Valid @RequestBody WithdrawDto dto) {

        User user = authenticationHelper.tryGetUser(headers);
        Wallet wallet = walletService.getById(id);
        Transfer transfer = transferMapper.fromWithdrawDto(dto);
        transferService.withdraw(transfer, user);


        Double requestBody = dto.getAmount();

        return webClientBuilder.build()
                .post()
                .uri("http://localhost:8081/withdraw")
                .bodyValue(requestBody)
                .retrieve()
                .toEntity(String.class)
                .map(responseEntity -> {
                    HttpStatus status = responseEntity.getStatusCode();
                    String responseBody = responseEntity.getBody();
                    return new ResponseEntity<>(responseBody, status);
                });
    }


}
