package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.TransactionMapper;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.TransactionDto;
import com.example.virtualwallet.services.contracts.EmailService;
import com.example.virtualwallet.services.contracts.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("api/transactions")
@Api(tags = "Transaction REST")
public class TransactionController {
    private final AuthenticationHelper authenticationHelper;
    private final TransactionService transactionService;
    private final EmailService emailService;
    private final TransactionMapper transactionMapper;

    public TransactionController(AuthenticationHelper authenticationHelper, TransactionService transactionService, EmailService emailService, TransactionMapper transactionMapper) {
        this.authenticationHelper = authenticationHelper;
        this.transactionService = transactionService;
        this.emailService = emailService;
        this.transactionMapper = transactionMapper;
    }

    @ApiOperation("Get transactions")
    @GetMapping
    public List<TransactionDto> get() {
        List<Transaction> transactions = transactionService.getAll();
        return transactionMapper.toDto(transactions);
    }

    @ApiOperation("Get transaction by ID")
    @GetMapping("/{id}")
    public TransactionDto getById(@PathVariable int id) {
        try {
            Transaction transaction = transactionService.getById(id);
            return transactionMapper.toDto(transaction);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation("Create transactions")
    @PostMapping
    public String create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TransactionDto transactionDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Transaction transaction = transactionMapper.fromDto(transactionDto);
            if (transaction.getAmount() < 1000) {
                transactionService.create(transaction, user);
                return "Transaction successfully initiated.";
            } else {
                String verificationCode = generateVerificationCode();
                transaction.setVerificationCode(verificationCode);
                transactionService.create(transaction, user);
                emailService.sendLargeTransactionVerificationEmail(user.getEmail(), verificationCode);
                return "Please confirm the transaction using the code sent to your E-Mail address.";
            }
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation("Verify transactions")
    @PostMapping("/verify")
    public String verify(@RequestHeader HttpHeaders headers, @RequestBody String verificationCode) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Transaction transaction = transactionService.getByCode(verificationCode);
            transactionService.verify(transaction, user, verificationCode);
            return "Transaction successfully executed";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    public static String generateVerificationCode() {
        Random random = new Random();
        int code = random.nextInt(900000) + 100000; // Generates a random number between 100000 and 999999
        return String.valueOf(code);
    }

}
