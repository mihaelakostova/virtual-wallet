package com.example.virtualwallet.controllers.rest;

import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.InvalidTokenException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.UserMapper;
import com.example.virtualwallet.models.Invitation;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.UserDto;
import com.example.virtualwallet.services.contracts.InvitationService;
import com.example.virtualwallet.services.contracts.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/invitations")
@Api(tags = "Invitation REST")
public class InvitationController {

    private final InvitationService invitationService;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserMapper userMapper;

    public InvitationController(InvitationService invitationService, AuthenticationHelper authenticationHelper, UserService userService, UserMapper userMapper) {
        this.invitationService = invitationService;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @ApiOperation("Send invitation")
    @PostMapping()
    public String sendInvitation(@RequestHeader HttpHeaders headers, @RequestBody String email) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            invitationService.createInvitation(user, email);
            return "Invitation sent successfully";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
    @ApiOperation("Register with invitation")
    @PostMapping("/register")
    public String registerWithInvitation(@RequestParam String token, @Valid @RequestBody UserDto userDto) {
        try {
            String rec = "recaptcha";
            Invitation invitation = invitationService.validateInvitationToken(token);
            User user = userMapper.fromDto(userDto);
            userService.createFromInvitation(user, invitation, rec);
            return "Registration completed successfully. Please check your inbox for your activation link.";
        } catch (InvalidTokenException e) {
            throw new ResponseStatusException(HttpStatus.GONE, e.getMessage());
        } catch (DuplicateEntityException e)  {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
