package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthenticationFailureException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.exceptions.UnverifiedUserException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.TransactionMapper;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.TransactionDto;
import com.example.virtualwallet.services.contracts.EmailService;
import com.example.virtualwallet.services.contracts.TransactionService;
import com.example.virtualwallet.services.contracts.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/users/{id}/transactions")
@Api(tags = "Transaction MVC")
public class TransactionMvcController {

    private final TransactionService transactionService;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final TransactionMapper transactionMapper;
    private final EmailService emailService;


    @Autowired
    public TransactionMvcController(TransactionService transactionService, AuthenticationHelper authenticationHelper, UserService userService, TransactionMapper transactionMapper, EmailService emailService) {
        this.transactionService = transactionService;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.transactionMapper = transactionMapper;
        this.emailService = emailService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("loggedInUser")
    public User getCurrentUser(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            return authenticationHelper.tryGetUser(httpSession);
        }
        return null;
    }

    @ModelAttribute("isBlocked")
    public boolean populateIsBlocked(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.isBlocked();
        }
        return false;
    }

    @ApiOperation("Show transactions")
    @GetMapping()
    public String showTransactionsPage(@PathVariable int id, HttpSession httpSession, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);

            if (user.getId() == id) {
                List<Transaction> transactions = transactionService.getAllFromUser(user);
                model.addAttribute("transactions", transactions);
                model.addAttribute("transactionDto", new TransactionDto());

                return "TransactionsViewTest";
            }
            return "AccessDeniedView";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @ApiOperation("Show transaction by ID")
    @GetMapping("/{transactionId}")
    public String showSingleTransaction(@PathVariable int id, @PathVariable int transactionId, Model model, HttpSession httpSession) {
        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            User loggedUser = authenticationHelper.tryGetUser(httpSession);
            if (loggedUser.getId() == id) {
                Transaction transaction = transactionService.getById(transactionId);
                model.addAttribute("transaction", transaction);
                return "TransactionView";
            }
            return "AccessDeniedView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @ApiOperation("Show new transaction")
    @GetMapping("/new")
    public String showNewTransactionPage(@PathVariable int id, HttpSession httpSession, Model model) {
        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        User user = authenticationHelper.tryGetUser(httpSession);
        if (user.getId() == id) {
            model.addAttribute("transactionDto", new TransactionDto());
            return "InitiateTransactionView";
        }
        return "AccessDeniedView";
    }

    @ApiOperation("Create transactions")
    @PostMapping("/new")
    public String initiateTransaction(@PathVariable int id, @ModelAttribute("transactionDto") @Valid TransactionDto transactionDto,
                                      HttpSession httpSession, Model model, BindingResult bindingResult) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);

            if (user.getId() == id) {
                if (bindingResult.hasErrors()) {
                    return "InitiateTransactionView";
                }
                Transaction transaction = transactionMapper.fromDto(transactionDto);
                if (transaction.getAmount() < 1000) {
                    transactionService.create(transaction, user);
                    model.addAttribute("transaction", transaction);
                    return "ConfirmTransactionView";
                } else {
                    String verificationCode = generateVerificationCode();
                    transaction.setVerificationCode(verificationCode);
                    transactionService.create(transaction, user);
                    emailService.sendLargeTransactionVerificationEmail(user.getEmail(), verificationCode);
                    model.addAttribute("transaction", transaction);
                    return "VerifyTransactionView";
                }
            }
            return "AccessDeniedView";

        } catch (AuthenticationFailureException e) {

            return "redirect:/auth/login";

        } catch (UnauthorizedOperationException | UnverifiedUserException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "InitiateTransactionView";
        }
    }

    @ApiOperation("Resend Code for verification")
    @PostMapping("/{transactionId}/resend")
    public String resendCode(@PathVariable int id,
                             @PathVariable int transactionId,
                             HttpSession httpSession, Model model, RedirectAttributes redirectAttributes) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);

            if (user.getId() == id) {
                String verificationCode = generateVerificationCode();
                Transaction transaction = transactionService.getById(transactionId);
                transaction.setVerificationCode(verificationCode);
                emailService.sendLargeTransactionVerificationEmail(user.getEmail(), verificationCode);
                model.addAttribute("success", "A new code has been sent to your e-mail address");
                model.addAttribute("transaction", transaction);
                redirectAttributes.addAttribute("resend", "true");
                return String.format("redirect:/users/{%s}/transactions/{%s}", id, transactionId);
            }

        } catch (AuthenticationFailureException e) {
            {
                return "redirect:/auth/login";
            }
        }
        return "AccessDeniedView";
    }

    @ApiOperation("Verify verification")
    @PostMapping("/verify")
    public String verifyTransaction(@PathVariable int id,
                                    @ModelAttribute("transaction") Transaction transaction,
                                    @RequestParam("verificationCode") String inputVerificationCode,
                                    HttpSession httpSession, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            Transaction verifiedTransaction = transactionService.getByCode(inputVerificationCode);
            transactionService.verify(verifiedTransaction, user, inputVerificationCode);
            return "TransactionSuccessfulView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", "Invalid verification code");
            return "VerifyTransactionView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "VerifyTransactionView";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @ApiOperation("Confirm transaction")
    @PostMapping("/{transactionId}/confirm")
    public String confirmTransaction(@PathVariable int id, @PathVariable int transactionId,
                                     @ModelAttribute("transaction") Transaction transaction,
                                     HttpSession httpSession,
                                     Model model) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);

            if (user.getId() == id) {
                Transaction currentTransaction = transactionService.getById(transactionId);
                transactionService.execute(currentTransaction);
                return "TransactionSuccessfulView";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "AccessDeniedView";
        }
        return "AccessDeniedView";
    }

    @ApiOperation("Cancel transaction")
    @GetMapping("/{transactionId}/cancel")
    public String cancelTransaction(@PathVariable int id, @PathVariable int transactionId,
                                    @ModelAttribute("transaction") Transaction transaction,
                                    HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);

            if (user.getId() == id) {
                Transaction currentTransaction = transactionService.getById(transactionId);
                transactionService.cancel(currentTransaction);
                return String.format("redirect:/users/%s/transactions", id);
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        return "AccessDeniedView";
    }

    private String generateVerificationCode() {
        Random random = new Random();
        int code = random.nextInt(900000) + 100000; // Generates a random number between 100000 and 999999
        return String.valueOf(code);
    }

}
