package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthenticationFailureException;
import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.Invitation;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.TransactionDto;
import com.example.virtualwallet.services.contracts.InvitationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/users/{id}/referrals")
@Api(tags = "Referral MVC")
public class ReferralsMvcController {
    private final InvitationService invitationService;
    private final AuthenticationHelper authenticationHelper;

    public ReferralsMvcController(InvitationService invitationService, AuthenticationHelper authenticationHelper) {
        this.invitationService = invitationService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("loggedInUser")
    public User getCurrentUser(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            return authenticationHelper.tryGetUser(httpSession);
        }
        return null;
    }

    @ModelAttribute("isBlocked")
    public boolean populateIsBlocked(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.isBlocked();
        }
        return false;
    }

    @ApiOperation("Show new referral page")
    @GetMapping()
    public String showNewReferralPage(@PathVariable int id, HttpSession httpSession, Model model) {
        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        User user = authenticationHelper.tryGetUser(httpSession);
        if (user.getId() == id) {
            model.addAttribute("invitation", new Invitation());
            return "ReferAFriendView";
        }
        return "AccessDeniedView";
    }

    @ApiOperation("Send invitation")
    @PostMapping()
    public String sendInvitation(HttpSession httpSession,
                                 @ModelAttribute("invitation") Invitation invitation,
                                 Model model,
                                 @PathVariable int id) {
        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        User user = authenticationHelper.tryGetUser(httpSession);
        try {
            invitationService.createInvitation(user, invitation.getEmail());
            model.addAttribute("success", "Referral link sent successfully!");
            return "ReferAFriendView";
        } catch (DuplicateEntityException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "ReferAFriendView";
        }
    }
}
