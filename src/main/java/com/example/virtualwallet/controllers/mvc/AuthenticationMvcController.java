package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.*;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.UserMapper;
import com.example.virtualwallet.models.Invitation;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.LoginDto;
import com.example.virtualwallet.models.dtos.RegisterDto;
import com.example.virtualwallet.models.emus.Role;
import com.example.virtualwallet.services.contracts.InvitationService;
import com.example.virtualwallet.services.contracts.RecaptchaService;
import com.example.virtualwallet.services.contracts.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
@Api(tags = "Authentication MVC")
public class AuthenticationMvcController {

    public static final String G_RECAPTCHA_RESPONSE = "g-recaptcha-response";
    public static final String REGISTER = "/register";
    private static final String CREATE_USER_DTO = "createRegisterDto";


    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserMapper userMapper;
    private final RecaptchaService recaptchaService;
    private final InvitationService invitationService;


    @Autowired
    public AuthenticationMvcController(AuthenticationHelper authenticationHelper,
                                       UserService userService,
                                       UserMapper userMapper,
                                       RecaptchaService recaptchaService, InvitationService invitationService) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userMapper = userMapper;
        this.recaptchaService = recaptchaService;
        this.invitationService = invitationService;
    }

    @ApiOperation("Show register page")
    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        boolean isReferred = false;
        model.addAttribute("isReferred", isReferred);
        return "RegisterUserView";
    }

    @ApiOperation("Show referral register page")
    @GetMapping("/referral")
    public String showReferralPage(@RequestParam("token") String referralToken, Model model) {
        model.addAttribute("register", new RegisterDto());
        boolean isReferred = true;
        model.addAttribute("isReferred", isReferred);
        model.addAttribute("referralToken", referralToken);
        return "RegisterUserView";
    }

    @ApiOperation("Register user")
    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto user,
                                 @RequestParam(G_RECAPTCHA_RESPONSE) String recaptchaResponse,
                                 BindingResult bindingResult,
                                 HttpServletRequest request,
                                 Model model) {
        if (bindingResult.hasErrors()) {
            prepareUserRegistrationPage(model, user, request);
            return "RegisterUserView";
        }

        if (!user.getPassword().equals(user.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm",
                    "password_error",
                    "Password confirmation should match password.");
            return "RegisterUserView";
        }

        try {
            User newUser = userMapper.fromDto(user);
            userService.create(newUser, recaptchaResponse);
            model.addAttribute("user", newUser);
            return "RegistrationSuccessfulView";
        } catch (DuplicateEmailException e) {
            bindingResult.rejectValue("email", "user.exists", e.getMessage());
            return "RegisterUserView";
        } catch (DuplicateUsernameException e) {
            bindingResult.rejectValue("username", "user.exists", e.getMessage());
            return "RegisterUserView";
        }
    }

    @ApiOperation("Register referral user")
    @PostMapping("/referral")
    public String handleRegisterWithReferral(@Valid @ModelAttribute("register") RegisterDto user,
                                             @RequestParam(G_RECAPTCHA_RESPONSE) String recaptchaResponse,
                                             @RequestParam("token") String referralToken,
                                             BindingResult bindingResult,
                                             HttpServletRequest request,
                                             Model model) {
        if (bindingResult.hasErrors()) {
            prepareUserRegistrationPage(model, user, request);
            return "RegisterUserView";
        }

        if (!user.getPassword().equals(user.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm",
                    "password_error",
                    "Password confirmation should match password.");
            return "RegisterUserView";
        }

        try {
            Invitation invitation = invitationService.validateInvitationToken(referralToken);

            if (invitation == null || !invitation.isValid()) {
                bindingResult.rejectValue("token", "invalid_token", "Invalid or expired referral token.");
                return "RegisterUserView";
            }

            User newUser = userMapper.fromDto(user);

            userService.createFromInvitation(newUser, invitation, recaptchaResponse);

            model.addAttribute("user", newUser);
            return "RegistrationSuccessfulView";
        } catch (InvalidTokenException e) {
            bindingResult.rejectValue("token", "invalid_token", "Invalid or expired referral token.");
            return "RegisterUserView";
        } catch (DuplicateEmailException e) {
            bindingResult.rejectValue("email", "user.exists", e.getMessage());
            return "RegisterUserView";
        } catch (DuplicateUsernameException e) {
            bindingResult.rejectValue("username", "user.exists", e.getMessage());
            return "RegisterUserView";
        }
    }

    @ApiOperation("Confirm account of user")
    @GetMapping("/confirm")
    public String confirmAccount(@RequestParam String token) {
        try {
            userService.verifyAccount(token);

            return "ConfirmationSuccessfulView";
        } catch (UnauthorizedOperationException e) {
            return "InvalidTokenView";
        }
    }

    @ApiOperation("Show login page")
    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "LoginUserView";
    }

    @ApiOperation("Login")
    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto loginDto,
                              BindingResult bindingResult,
                              HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            return "LoginUserView";
        }
        try {
            authenticationHelper.verifyAuthentication(loginDto.getUsername(), loginDto.getPassword());
            User loggedInUser = userService.getByUsername(loginDto.getUsername());
            httpSession.setAttribute("currentUser", loginDto.getUsername());
            if (loggedInUser.getRole() == Role.ADMIN) {
                return "redirect:/home";
            } else {
                return "redirect:/wallets";
            }
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "LoginUserView";
        }
    }

    @ApiOperation("Logout")
    @GetMapping("/logout")
    public String handleLogOut(HttpSession httpSession) {
        httpSession.removeAttribute("currentUser");
        return "redirect:/home";
    }

    @Nullable
    private String getRegisterFirstMessage(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        String attribute = null;
        if (session != null) {
            attribute = (String) session.getAttribute(REGISTER);
        }
        return attribute;
    }

    private void prepareUserRegistrationPage(Model model,
                                             RegisterDto dto,
                                             HttpServletRequest request) {
        String msg = getRegisterFirstMessage(request);
        model.addAttribute(CREATE_USER_DTO, dto);
    }

}
