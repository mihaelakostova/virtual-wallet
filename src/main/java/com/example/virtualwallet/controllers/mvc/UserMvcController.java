package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthenticationFailureException;
import com.example.virtualwallet.exceptions.DuplicateEmailException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.UserMapper;
import com.example.virtualwallet.models.Image;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.EditUserDto;
import com.example.virtualwallet.models.dtos.UserFilterDto;
import com.example.virtualwallet.models.emus.ImageType;
import com.example.virtualwallet.models.emus.Role;
import com.example.virtualwallet.models.filters.UserFilterOptions;
import com.example.virtualwallet.services.contracts.ImageService;
import com.example.virtualwallet.services.contracts.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequestMapping("/users")
@Api(tags = "User MVC")
public class UserMvcController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final ImageService imageService;

    @Autowired
    public UserMvcController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper, ImageService imageService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.imageService = imageService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("loggedInUser")
    public User getCurrentUser(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            return authenticationHelper.tryGetUser(httpSession);
        }
        return null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.getRole() == Role.ADMIN;
        }
        return false;
    }

    @ModelAttribute("isBlocked")
    public boolean populateIsBlocked(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.isBlocked();
        }
        return false;
    }

    @ApiOperation("Get user by id")
    @GetMapping("/{id}")
    public String showUserById(
            @PathVariable int id,
            @ModelAttribute("viewType")
            String viewType,
            Model model,
            HttpSession httpSession
    ) {
        try {
            User user = userService.getById(id);
            model.addAttribute("user", user);
            User loggedInUser = authenticationHelper.tryGetUser(httpSession);

            if (loggedInUser.getId() == id) {
                model.addAttribute("isProfileOwner", true);
            }
            boolean viewedUserIsBlocked = user.isBlocked();
            model.addAttribute("isBlocked", viewedUserIsBlocked);

            return "UserProfileView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthenticationFailureException e) {
            return "redirect:/users";
        }
    }

    @ApiOperation("Get all users")
    @GetMapping
    public String showAllUsers(@ModelAttribute("filter") UserFilterDto userFilterDto,
                               @RequestParam(name = "page", defaultValue = "1") int page,
                               Model model,
                               HttpServletRequest request) {
        int pageSize = 14;

        UserFilterOptions userFilterOptions = new UserFilterOptions(
                userFilterDto.getSearchTerm(),
                userFilterDto.getUsername(),
                userFilterDto.getName(),
                userFilterDto.getEmail(),
                userFilterDto.getPhone()
        );

        Page<User> userPage = userService.getAllPaged(userFilterOptions, PageRequest.of(page - 1, pageSize));

        model.addAttribute("users", userPage.getContent());
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", userPage.getTotalPages());

        String queryString = request.getQueryString();
        model.addAttribute("query", queryString);

        model.addAttribute("prevButtonClass", page == 1 ? "inactive" : "");
        model.addAttribute("nextButtonClass", page == userPage.getTotalPages() ? "inactive" : "");

        model.addAttribute("filter", userFilterDto);
        return "AllUsersView";
    }

    @ApiOperation("Set block user")
    @GetMapping("/{id}/block")
    public String blockUser(@PathVariable int id,
                            @ModelAttribute("user") User viewedUser,
                            Model model,
                            HttpSession httpSession) {
        try {
            User executingUser = authenticationHelper.tryGetUser(httpSession);
            if (executingUser.getRole() != Role.ADMIN) {
                return "AccessDeniedView";
            }
            User user = userService.getById(id);
            userService.blockUser(user);
            model.addAttribute("isBlocked", true);
            return "redirect:/users/{id}";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @ApiOperation("Get unblock user")
    @GetMapping("/{id}/unblock")
    public String unblockUser(@PathVariable int id,
                              @ModelAttribute("user") User viewedUser,
                              Model model,
                              HttpSession httpSession) {
        try {
            User executingUser = authenticationHelper.tryGetUser(httpSession);
            if (executingUser.getRole() != Role.ADMIN) {
                return "AccessDeniedView";
            }
            User user = userService.getById(id);
            userService.unblockUser(user);
            model.addAttribute("isBlocked", false);
            return "redirect:/users/{id}";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @ApiOperation("Show Update user")
    @GetMapping("/{id}/update")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession httpSession) {
        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            User loggedInUser = authenticationHelper.tryGetUser(httpSession);
            if (loggedInUser.getId() != id) {
                return "AccessDeniedView";
            }
            User user = userService.getById(id);
            EditUserDto userDto = userMapper.editToDto(user);
            model.addAttribute("update", userDto);
            return "UserUpdateView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @ApiOperation("Update user")
    @PostMapping("/{id}/update")
    public String updateUser(@PathVariable int id,
                             @Valid @ModelAttribute("update") EditUserDto user,
                             BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "UserUpdateView";
        }

        if (!user.getPassword().equals(user.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm",
                    "password_error",
                    "Password confirmation should match password.");
            return "UserUpdateView";
        }

        try {
            User newUser = userMapper.fromDto(id, user);
            userService.update(newUser);
            return "redirect:/users/{id}";
        } catch (DuplicateEmailException e) {
            bindingResult.rejectValue("email", "user.exists", e.getMessage());
            return "UserUpdateView";
        }
    }

    @ApiOperation("Uplode image by user")
    @PostMapping("{id}/image")
    public String uploadImage(@PathVariable int id,
                              @RequestParam("file") MultipartFile file,
                              @RequestParam("type") ImageType type,
                              HttpSession session,
                              Model model) throws IOException {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        User targetUser;
        try {
            targetUser = this.userService.getById(id);
        } catch (EntityNotFoundException e) {
            return "NotFoundView";
        }

        if (!loggedUser.getRole().equals(Role.ADMIN) &&
                !loggedUser.getUsername().equals(targetUser.getUsername())) {
            return "AccessDeniedView";
        }
        model.addAttribute("uploadSuccess", true);
        if (!file.isEmpty()) {
            Image oldImage = targetUser.getImage();
            if (oldImage != null) {
                imageService.destroyImage(oldImage, targetUser.getUsername());
            }

            String imgUrl = imageService.uploadImage(file.getBytes(), targetUser.getUsername());
            Image createImage = new Image();
            createImage.setImageUrl(imgUrl);
            createImage.setBlocked(true);
            createImage.setUser(loggedUser);
            createImage.setImageType(type);
            imageService.save(createImage);

            targetUser.setImage(createImage);
            userService.update(targetUser);

            if (oldImage != null) {
                this.imageService.delete(oldImage.getId());
            }
        }

        return "redirect:/users/" + id;
    }

}
