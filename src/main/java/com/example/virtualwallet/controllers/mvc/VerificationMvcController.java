package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthenticationFailureException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.models.Image;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.emus.Role;
import com.example.virtualwallet.services.contracts.ImageService;
import com.example.virtualwallet.services.contracts.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("verification")
@Api(tags = "Verification MVC")
public class VerificationMvcController {
    private final ImageService imageService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;

    public VerificationMvcController(ImageService imageService,
                                     UserService userService,
                                     AuthenticationHelper authenticationHelper) {
        this.imageService = imageService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("loggedInUser")
    public User getCurrentUser(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            return authenticationHelper.tryGetUser(httpSession);
        }
        return null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.getRole() == Role.ADMIN;
        }
        return false;
    }

    @ApiOperation("Show all block images")
    @GetMapping
    public String showAllBlockVerification(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
            if (user.getRole().equals(Role.USER)) {
                return "AccessDeniedView";
            }
        } catch (AuthenticationFailureException e) {
            return "RegisterOrLoginView";
        }
        List<Image> imageList = imageService.getAllBlocked();
        model.addAttribute("images", imageList);
        return "AllImageView";
    }

    @ApiOperation("Show single image")
    @GetMapping("/{id}")
    public String showSingleImage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "RegisterOrLoginView";
        }

        try {
            Image image = imageService.getImageById(id);
            model.addAttribute("image", image);
            return "ImageView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @ApiOperation("Show images by user")
    @GetMapping("/user/{id}")
    public String showImageByUser(@PathVariable int id, Model model, HttpSession session) {
        try {
            User logged = authenticationHelper.tryGetUser(session);
            if (!logged.getRole().equals(Role.ADMIN)) {

            }
        } catch (AuthenticationFailureException e) {
            return "RegisterOrLoginView";
        }

        try {
            User user = userService.getById(id);
            model.addAttribute("image", user.getImage());
            return "ImageView";

        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @ApiOperation("Change image status")
    @GetMapping("{id}/status")
    public String changeImageStatus(@PathVariable int id, HttpSession session, Model model) {
        User logUser;
        Image image;
        try {
            image = imageService.getImageById(id);
            logUser = authenticationHelper.tryGetUser(session);
            if (!logUser.getRole().equals(Role.ADMIN)) {
                model.addAttribute("error", "No access");
                return "AccessDeniedView";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }

        imageService.changeBlockStatus(image, logUser);
        return "redirect:/verification/" + id;
    }

    @ApiOperation("Change status of User")
    @GetMapping("{id}/verification")
    public String changeVerificationStatus(@PathVariable int id, HttpSession session, Model model) {
        User logUser;
        try {
            logUser = authenticationHelper.tryGetUser(session);
            if (!logUser.getRole().equals(Role.ADMIN)) {
                model.addAttribute("error", "No access");
                return "AccessDeniedView";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            User user = userService.getById(id);
            userService.changeVerificationStatus(user, logUser);
            return "redirect:/verification/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }


}
