package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.dtos.TransactionFilterDto;
import com.example.virtualwallet.models.filters.TransactionFilterOptions;
import com.example.virtualwallet.services.contracts.TransactionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/transactions")
@Api(tags = "Transaction MVC")
public class AllTransactionsMvcController {

    private final TransactionService transactionService;

    public AllTransactionsMvcController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @ApiOperation("Show all transactions")
    @GetMapping("/admin")
    public String showAllTransactionsPaged(@ModelAttribute("filter") TransactionFilterDto transactionFilterDto,
                                           @RequestParam(name = "page", defaultValue = "1") int page,
                                           Model model,
                                           HttpServletRequest request) {
        int pageSize = 14;

        TransactionFilterOptions transactionFilterOptions = new TransactionFilterOptions(

                transactionFilterDto.getSender(),
                transactionFilterDto.getRecipient(),
                transactionFilterDto.getType(),
                transactionFilterDto.getStatus(),
                transactionFilterDto.getMinAmount(),
                transactionFilterDto.getMaxAmount(),
                transactionFilterDto.getMinDate(),
                transactionFilterDto.getMaxDate(),
                transactionFilterDto.getSortBy(),
                transactionFilterDto.getSortOrder()
        );

        Page<Transaction> transactionPage = transactionService.getAllPaged(transactionFilterOptions, PageRequest.of(page - 1, pageSize));

        model.addAttribute("transactions", transactionPage.getContent());
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", transactionPage.getTotalPages());

        String queryString = request.getQueryString();
        model.addAttribute("query", queryString);

        model.addAttribute("prevButtonClass", page == 1 ? "inactive" : "");
        model.addAttribute("nextButtonClass", page == transactionPage.getTotalPages() ? "inactive" : "");

        model.addAttribute("filter", transactionFilterDto);
        return "AllTransactionsView";
    }
}
