package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthenticationFailureException;
import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.CardMapper;
import com.example.virtualwallet.helpers.TransferMapper;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.CardDto;
import com.example.virtualwallet.models.dtos.WithdrawDto;
import com.example.virtualwallet.services.contracts.CardService;
import com.example.virtualwallet.services.contracts.TransferService;
import com.example.virtualwallet.services.contracts.WalletService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("/cards")
@Api(tags = "Card MVC")
public class CardMvcController {

    private final CardService cardService;
    private final AuthenticationHelper authenticationHelper;
    private final CardMapper cardMapper;
    private final TransferService transferService;
    private final TransferMapper transferMapper;
    private final WalletService walletService;

    @Autowired
    public CardMvcController(CardService cardService, AuthenticationHelper authenticationHelper, CardMapper cardMapper, TransferService transferService, TransferMapper transferMapper, WalletService walletService) {
        this.cardService = cardService;
        this.authenticationHelper = authenticationHelper;
        this.cardMapper = cardMapper;
        this.transferService = transferService;
        this.transferMapper = transferMapper;
        this.walletService = walletService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("loggedInUser")
    public User getCurrentUser(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            return authenticationHelper.tryGetUser(httpSession);
        }
        return null;
    }

    @ModelAttribute("wallets")
    public Set<Wallet> populateWallets(HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            return user.getWallets();
        } catch (AuthenticationFailureException e) {
            return Collections.emptySet();
        }
    }

    @ApiOperation("Show my cards")
    @GetMapping()
    public String showMyCards(Model model, HttpSession session) {
        User logUser;
        try {
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }


        List<Card> cards = cardService.getByUser(logUser);
        model.addAttribute("cards", cards);
        return "MyCardsView";
    }

    @ApiOperation("Show card by ID")
    @GetMapping("/{id}")
    public String showSingleCard(@PathVariable int id, Model model, HttpSession session) {
        Card card;
        User logUser;
        try {
            card = cardService.getById(id);
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        if (!card.getUser().getUsername().equals(logUser.getUsername())) {
            model.addAttribute("error", "No access");
            return "AccessDenied";
        }
        model.addAttribute("card", card);
        return "CardView";

    }

    @ApiOperation("Show add card page")
    @GetMapping("/add")
    public String showAddCardPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("card", new CardDto());
        return "AddCardView";
    }

    @ApiOperation("Add cards")
    @PostMapping("/add")
    public String addCard(@Valid @ModelAttribute("card") CardDto cardDto,
                          BindingResult bindingResult,
                          Model model,
                          HttpSession session) {
        User logUser;
        try {
            logUser = authenticationHelper.tryGetUser(session);

        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "AddCardView";
        }

        try {
            Card card = cardMapper.fromDto(cardDto);
            cardService.create(card, logUser);
            return "redirect:/cards";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("card", "duplicate_card", e.getMessage());
            return "AddCardView";
        }
    }

    @ApiOperation("Delete card by Id")
    @GetMapping("/{id}/delete")
    public String deleteCard(@PathVariable int id, Model model, HttpSession session) {

        User logUser;
        Card card;
        try {
            card = cardService.getById(id);
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }

        if (!logUser.getUsername().equals(card.getUser().getUsername())) {
            model.addAttribute("error", "No access");
            return "AccessDenied";
        }
        cardService.delete(id, logUser);
        return "redirect:/cards";
    }

    @ApiOperation("Top up wallet from card")
    @GetMapping("/{id}/{walletId}/topUp")
    public String topUp(@PathVariable int id, @RequestParam int walletId, @ModelAttribute("amount") WithdrawDto dto, Model model, HttpSession session) {
        User logUser;
        try {
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (!logUser.getUsername().equals(cardService.getById(id).getUser().getUsername())) {
            model.addAttribute("error", "No access");
            return "AccessDeniedView";
        }
        try {
            Transfer transfer = transferMapper.fromWithdrawDto(dto);
            Wallet wallet = walletService.getById(walletId);
            transfer.setRecipient(wallet);
            transfer.setSender(cardService.getById(id));
            transferService.topUp(transfer, authenticationHelper.tryGetUser(session));
            return "redirect:/wallets/" + walletId;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @ApiOperation("Withdraw from wallet to card")
    @GetMapping("/{id}/{walletId}/withdraw")
    public String withdraw(@PathVariable int id,
                           @RequestParam int walletId,
                           @ModelAttribute("amount") WithdrawDto dto,
                           Model model,
                           HttpSession session,
                           BindingResult bindingResult) {

        User logUser;
        Card card;
        try {
            card = cardService.getById(id);
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        if (!logUser.getUsername().equals(cardService.getById(id).getUser().getUsername())) {
            model.addAttribute("error", "No access");
            return "AccessDeniedView";
        }

        Wallet wallet = walletService.getById(walletId);
        if (dto.getAmount() < 0 || dto.getAmount() > wallet.getBalance()) {
            bindingResult.rejectValue("amount", dto.getAmount().toString());
            return "redirect:/cards/" + id;
        }
        wallet.setBalance(wallet.getBalance() - dto.getAmount());
        return "redirect:/wallets/" + walletId;
    }

    @ApiOperation("Change status of card")
    @GetMapping("{id}/status")
    public String changeCardStatus(@PathVariable int id, HttpSession session, Model model) {
        User logUser;
        Card card;
        try {
            card = cardService.getById(id);
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        if (!card.getUser().getUsername().equals(logUser.getUsername())) {
            model.addAttribute("error", "No access");
            return "AccessDeniedView";
        }
        cardService.changeBlockStatus(card, logUser);
        return "redirect:/cards";
    }

}
