package com.example.virtualwallet.controllers.mvc;

import com.example.virtualwallet.exceptions.AuthenticationFailureException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.helpers.AuthenticationHelper;
import com.example.virtualwallet.helpers.CurrencyMapper;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.dtos.WalletFilterDTo;
import com.example.virtualwallet.models.emus.CurrencyType;
import com.example.virtualwallet.models.filters.WalletFilterOptions;
import com.example.virtualwallet.services.contracts.CurrencyExchangeService;
import com.example.virtualwallet.services.contracts.UserService;
import com.example.virtualwallet.services.contracts.WalletService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("wallets")
@Api(tags = "Wallet MVC")
public class WalletMvcController {

    private final WalletService walletService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final CurrencyExchangeService currencyExchangeService;
    private final CurrencyMapper currencyMapper;

    @Autowired
    public WalletMvcController(WalletService walletService,
                               UserService userService,
                               AuthenticationHelper authenticationHelper,
                               CurrencyExchangeService currencyExchangeService,
                               CurrencyMapper currencyMapper) {
        this.walletService = walletService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.currencyExchangeService = currencyExchangeService;
        this.currencyMapper = currencyMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("loggedInUser")
    public User getCurrentUser(HttpSession httpSession) {
        if (populateIsAuthenticated(httpSession)) {
            return authenticationHelper.tryGetUser(httpSession);
        }
        return null;
    }

    @ApiOperation("Get all wallets")
    @GetMapping()
    public String ShowMyWallets(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "RegisterOrLoginView";
        }
        Set<Wallet> walletsByUser = user.getWallets();
        model.addAttribute("wallets", walletsByUser);
        return "MyWalletsView";
    }

    @ApiOperation("Get wallet by id")
    @GetMapping("/{id}")
    public String showSingleWallet(@PathVariable int id, Model model, HttpSession session) {
        User logUser;
        Wallet wallet;
        try {
            wallet = walletService.getById(id);
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        if (!(wallet.getHolders().contains(logUser))) {
            return "AccessDenied";
        }
        model.addAttribute("wallet", wallet);
        return "WalletView";
    }

    @ApiOperation("Get filter wallets")
    @GetMapping("/All")
    public String showAll(Model model, @ModelAttribute("filter") WalletFilterDTo dto, HttpSession session) {
        WalletFilterOptions walletFilterOptions = new WalletFilterOptions(dto.getNumber(),
                dto.getMinBalance(),
                dto.getMaxBalance(),
                dto.getSortBy(),
                dto.getSortOrder());
        User logUser;
        try {
            logUser = authenticationHelper.tryGetUser(session);
            if (!(logUser.getRole().toString().equals("Admin"))) {
                model.addAttribute("error", "No access");
                return "AccessDenied";
            }
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        List<Wallet> wallets = walletService.getAll(walletFilterOptions);
        model.addAttribute("wallets", wallets);
        return "AllWalletsView";
    }

    @ApiOperation("Create wallet")
    @PostMapping()
    public String create(HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        walletService.create(user);
        return "redirect:/wallets";
    }

    @ApiOperation("Delete wallet")
    @GetMapping("/{id}/delete")
    public String delete(@PathVariable int id, HttpSession session, Model model) {
        User logUser;
        Wallet wallet;
        try {
            wallet = walletService.getById(id);
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        if (!logUser.getUsername().equals(wallet.getCreator().getUsername())) {
            model.addAttribute("error", "No access");
            return "AccessDenied";
        }
        walletService.delete(id, logUser);
        return "redirect:/wallets";
    }

    @ApiOperation("Add user to wallets")
    @PostMapping("{id}/AddUser")
    public String addUserToWallet(@PathVariable int id,
                                  @RequestParam("searchTerm") String searchTerm,
                                  HttpSession session,
                                  Model model) {
        Wallet wallet;
        User logUser;
        try {
            wallet = walletService.getById(id);
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        if (!logUser.getUsername().equals(wallet.getCreator().getUsername())) {
            model.addAttribute("error", "No access");
            return "AccessDenied";
        }

        try {
            User userToAdd = userService.findByUsernameOrEmailOrPhone(searchTerm);
            walletService.addUserToWallet(logUser, userToAdd, wallet);
            return "redirect:/wallets";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }

    }

    @ApiOperation("Remove user from wallets")
    @PostMapping("{id}/RemoveUser")
    public String removeUserToWallet(@PathVariable int id,
                                     @RequestParam("username") String username,
                                     HttpSession session,
                                     Model model) {
        Wallet wallet;
        User logUser;
        try {
            wallet = walletService.getById(id);
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        if (!logUser.getUsername().equals(walletService.getById(id).getCreator().getUsername())) {
            model.addAttribute("error", "No access");
            return "AccessDenied";
        }
        Set<User> users = wallet.getHolders();
        model.addAttribute("users", users);
        model.addAttribute("searchTermNotFound", true);
        User userToRemove = userService.getByUsername(username);
        walletService.removeUserFromWallet(logUser, userToRemove, wallet);
        return "redirect:/wallets";

    }

    @ApiOperation("Change stauts of wallet ")
    @GetMapping("{id}/status")
    public String changeWalletStatus(@PathVariable int id, HttpSession session, Model model) {
        User logUser;
        Wallet wallet;
        try {
            wallet = walletService.getById(id);
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
        if (!wallet.getCreator().getUsername().equals(logUser.getUsername())) {
            model.addAttribute("error", "No access");
            return "AccessDeniedView";
        }
        walletService.changeBlockStatus(wallet, logUser);
        return "redirect:/wallets";
    }

    @ApiOperation("Change currency of wallet")
    @GetMapping("{id}/currency")
    public String changeWalletCurrency(@PathVariable int id,
                                       @RequestParam(name = "type") String currencyCode,
                                       HttpSession session,
                                       Model model) {
        User logUser;
        Wallet wallet;
        try {
            wallet = walletService.getById(id);
            logUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }

        if (!wallet.getHolders().contains(logUser)) {
            return "AccessDenied";
        }
        model.addAttribute("selectedCurrency", wallet.getCurrency());
        CurrencyType selectedCurrency = currencyMapper.mapCurrencyCodeToEnum(currencyCode);
        double convertBalance = currencyExchangeService.exchange(wallet.getCurrency(), selectedCurrency, wallet.getBalance());
        wallet.setCurrency(selectedCurrency);
        wallet.setBalance(convertBalance);
        walletService.update(wallet, logUser);
        return "redirect:/wallets/" + id;
    }


}
