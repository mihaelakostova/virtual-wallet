package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.dtos.TransactionDto;
import com.example.virtualwallet.models.dtos.TransferDto;
import com.example.virtualwallet.models.dtos.WithdrawDto;
import com.example.virtualwallet.services.contracts.CardService;
import com.example.virtualwallet.services.contracts.WalletService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
@Component
public class TransferMapper {
    private final WalletService walletService;
    private final CardService cardService;

    public TransferMapper(WalletService walletService, CardService cardService) {
        this.walletService = walletService;
        this.cardService = cardService;
    }
    public TransferDto toDto (Transfer transfer){
        TransferDto transferDto = new TransferDto();
        transferDto.setAmount(transfer.getAmount());
        transferDto.setSenderNumber(transfer.getSender().getNumber());
        transferDto.setRecipientNumber(transfer.getRecipient().getNumber());
        transferDto.setDate(transfer.getDate());
        return transferDto;
    }
    public List<TransferDto> toDto (List<Transfer> transfers){
        List<TransferDto> transferDtos = new ArrayList<>();
        for (Transfer transfer : transfers) {
            transferDtos.add(toDto(transfer));
        }
        return transferDtos;
    }

    public Transfer fromDto (TransferDto transferDto){
        Transfer transfer = new Transfer();
        transfer.setRecipient(walletService.getByNumber(transferDto.getRecipientNumber()));
        transfer.setSender(cardService.getByNumber(transferDto.getSenderNumber()));
        transfer.setAmount(transferDto.getAmount());
        transfer.setDate();
        return transfer;
    }

    public Transfer fromWithdrawDto (WithdrawDto withdrawDto){
        Transfer transfer = new Transfer();
        transfer.setAmount(withdrawDto.getAmount());

        transfer.setDate();
        return transfer;
    }
}
