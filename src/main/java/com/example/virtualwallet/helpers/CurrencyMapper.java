package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.emus.CurrencyType;
import org.springframework.stereotype.Component;

@Component
public class CurrencyMapper {

    public CurrencyType mapCurrencyCodeToEnum(String currencyCode) {
        switch (currencyCode) {
            case "USD":
                return CurrencyType.USD;
            case "EUR":
                return CurrencyType.EUR;
            case "BGN":
                return CurrencyType.BGN;
            default:
                return null;
        }
    }

}
