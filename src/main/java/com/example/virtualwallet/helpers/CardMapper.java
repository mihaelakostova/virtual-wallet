package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.dtos.CardDto;
import com.example.virtualwallet.services.contracts.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CardMapper {

    private final CardService cardService;

    @Autowired
    public CardMapper(CardService cardService) {
        this.cardService = cardService;
    }


    public Card fromDto(int id, CardDto dto){
        Card card = fromDto(dto);
        card.setCard_id(id);
        Card cardRepo = cardService.getById(id);
        card.setUser(cardRepo.getUser());
        return card;
    }

    public Card fromDto(CardDto dto){
        Card card = new Card();
        card.setCheck_number(dto.getCheck_number());
        card.setNumber(dto.getNumber());
        card.setMonth(dto.getMonth());
        card.setYear(dto.getYear());
        card.setType(dto.getType());
        return card;
    }

    public  CardDto toDto(Card card){
        CardDto cardDto = new CardDto();
        cardDto.setCheck_number(card.getCheck_number());
        cardDto.setNumber(card.getNumber());
        cardDto.setMonth(card.getMonth());
        cardDto.setYear(card.getYear());
        cardDto.setType(card.getType());
        return cardDto;
    }


    public List<CardDto> toDtoList(List<Card> cards){
        List<CardDto> cardDtoList = new ArrayList<>();
        for (Card card :cards) {
            cardDtoList.add(toDto(card));
        }
        return cardDtoList;
    }
}
