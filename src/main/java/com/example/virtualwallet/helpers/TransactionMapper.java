package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.dtos.CardDto;
import com.example.virtualwallet.models.dtos.TransactionDto;
import com.example.virtualwallet.models.emus.TransactionStatus;
import com.example.virtualwallet.models.emus.TransactionType;
import com.example.virtualwallet.services.contracts.TransactionService;
import com.example.virtualwallet.services.contracts.WalletService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class TransactionMapper {
    private final WalletService walletService;

    public TransactionMapper(WalletService walletService) {
        this.walletService = walletService;
    }
    public TransactionDto toDto (Transaction transaction){
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setAmount(transaction.getAmount());
        transactionDto.setSenderNumber(transaction.getSender().getNumber());
        transactionDto.setRecipientNumber(transaction.getRecipient().getNumber());
//        transactionDto.setDate(transaction.getDate());
        transactionDto.setType(transaction.getType().toString());
        return transactionDto;
    }
    public List<TransactionDto> toDto (List<Transaction> transactions){
        List<TransactionDto> transactionDtos = new ArrayList<>();
        for (Transaction transaction : transactions) {
            transactionDtos.add(toDto(transaction));
        }
        return transactionDtos;
    }

    public Transaction fromDto (TransactionDto transactionDto){
        Transaction transaction = new Transaction();
        transaction.setRecipient(walletService.getByNumber(transactionDto.getRecipientNumber()));
        transaction.setSender(walletService.getByNumber(transactionDto.getSenderNumber()));
        transaction.setAmount(transactionDto.getAmount());
        transaction.setDate(LocalDateTime.now());
        transaction.setType(TransactionType.OUTGOING);
        transaction.setStatus(TransactionStatus.PENDING);
        return transaction;
    }
}
