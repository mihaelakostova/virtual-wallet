package com.example.virtualwallet.helpers;

import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.dtos.EditUserDto;
import com.example.virtualwallet.models.dtos.RegisterDto;
import com.example.virtualwallet.models.dtos.UserDto;
import com.example.virtualwallet.models.emus.Role;
import com.example.virtualwallet.models.emus.UserStatus;
import com.example.virtualwallet.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    private final UserService userService;

    @Autowired
    public UserMapper(UserService userService) {
        this.userService = userService;

    }

    public User fromDto(int id, UserDto dto) {
        User user = fromDto(dto);
        user.setId(id);
//        User repositoryUser = userService.getById(id);
        return user;
    }

    public User fromDto(UserDto dto) {
        User user = new User();
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setPassword(dto.getPassword());
        user.setName(dto.getName());
        user.setEmail(dto.getEmail());
        user.setPhone(dto.getPhone());
        user.setUserStatus(UserStatus.UNVERIFIED);
        return user;
    }

    public User fromRegisterDto(RegisterDto dto) {
        User user = new User();
        user.setName(dto.getName());
        user.setUsername(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setRole(Role.USER);
        user.setName(dto.getName());
        user.setEmail(dto.getEmail());

        user.setPhone(dto.getPhone());
        return user;
    }


    public User fromDto(int userId, EditUserDto editProfileDto){
        User user = userService.getById(userId);
        dtoToObject(editProfileDto, user);
        return user;
    }

    public EditUserDto editToDto(User user){
        EditUserDto userDto = new EditUserDto();
        userDto.setEmail(user.getEmail());
        userDto.setPassword(user.getPassword());
        userDto.setPhone(user.getPhone());
        userDto.setPasswordConfirm(user.getPassword());
        return userDto;
    }

    private void dtoToObject(EditUserDto editUserDto, User user){
        user.setEmail(editUserDto.getEmail());
        user.setPhone(editUserDto.getPhone());
        user.setPassword(editUserDto.getPassword());
    }
}
