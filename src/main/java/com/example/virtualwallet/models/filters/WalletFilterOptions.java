package com.example.virtualwallet.models.filters;

import java.util.Optional;

public class WalletFilterOptions {

    private Optional<String> number;
    private Optional<Double> minBalance;
    private Optional<Double> maxBalance;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public WalletFilterOptions() {
        this(null, null, null, null, null);
    }

    public WalletFilterOptions(
            String name,
            Double minAbv,
            Double maxAbv,
            String sortBy,
            String sortOrder) {
        this.number = Optional.ofNullable(name);
        this.minBalance = Optional.ofNullable(minAbv);
        this.maxBalance = Optional.ofNullable(maxAbv);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<String> getNumber() {
        return number;
    }

    public Optional<Double> getMinBalance() {
        return minBalance;
    }

    public Optional<Double> getMaxBalance() {
        return maxBalance;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
