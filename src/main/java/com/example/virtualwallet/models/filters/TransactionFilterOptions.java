package com.example.virtualwallet.models.filters;

import java.time.LocalDateTime;
import java.util.Optional;

public class TransactionFilterOptions {

    private Optional<Integer> sender;

    private Optional<Integer> recipient;

    private Optional<String> type;

    private Optional<String> status;

    private Optional<Double> minAmount;

    private Optional<Double> maxAmount;

    private Optional<LocalDateTime> minDate;

    private Optional<LocalDateTime> maxDate;

    private Optional<String> sortBy;

    private Optional<String> sortOrder;

    public TransactionFilterOptions(){
        this(null, null, null, null, null, null, null, null, null, null);
    }

    public TransactionFilterOptions(
                            Integer sender,
                            Integer recipient,
                            String type,
                            String status,
                            Double minAmount,
                            Double maxAmount,
                            LocalDateTime minDate,
                            LocalDateTime maxDate,
                            String sortBy,
                            String sortOrder){
        this.sender = Optional.ofNullable(sender);
        this.recipient = Optional.ofNullable(recipient);
        this.type = Optional.ofNullable(type);
        this.status = Optional.ofNullable(status);
        this.minAmount = Optional.ofNullable(minAmount);
        this.maxAmount = Optional.ofNullable(maxAmount);
        this.minDate = Optional.ofNullable(minDate);
        this.maxDate = Optional.ofNullable(maxDate);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<Double> getMinAmount() {
        return minAmount;
    }

    public Optional<Double> getMaxAmount() {
        return maxAmount;
    }

    public Optional<Integer> getSender() {
        return sender;
    }

    public Optional<Integer> getRecipient() {
        return recipient;
    }

    public Optional<LocalDateTime> getMinDate() {
        return minDate;
    }

    public Optional<LocalDateTime> getMaxDate() {
        return maxDate;
    }

    public Optional<String> getType() {
        return type;
    }

    public Optional<String> getStatus() {
        return status;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
