package com.example.virtualwallet.models.filters;
import java.util.Optional;

public class CardFilterOptions {

    private Optional<String> number;
    private Optional<String> check_number;
    private Optional<String> sortBy;
    private Optional<String> orderBy;

    public CardFilterOptions(String number, String check_number,  String sortBy, String orderBy) {
        this.number = Optional.ofNullable(number);
        this.check_number = Optional.ofNullable(check_number);
        this.sortBy = Optional.ofNullable(sortBy);
        this.orderBy = Optional.ofNullable(orderBy);
    }

    public Optional<String> getNumber() {
        return number;
    }

    public Optional<String> getCheck_number() {
        return check_number;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getOrderBy() {
        return orderBy;
    }
}
