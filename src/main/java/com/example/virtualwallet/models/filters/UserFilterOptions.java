package com.example.virtualwallet.models.filters;

import java.util.Optional;

public class UserFilterOptions {

    private Optional<String> searchTerm;

    private Optional<String> name;

    private Optional<String> username;

    private Optional<String> email;

    private Optional<String> phone;

    private Optional<String> sortBy;

    private Optional<String> sortOrder;

    public UserFilterOptions() {
        this(null, null, null, null, null, null, null);
    }

    public UserFilterOptions(String searchTerm,
                             String name,
                             String username,
                             String email,
                             String phone,
                             String sortBy,
                             String sortOrder) {
        this.searchTerm = Optional.ofNullable(searchTerm);
        this.name = Optional.ofNullable(name);
        this.username = Optional.ofNullable(username);
        this.email = Optional.ofNullable(email);
        this.phone = Optional.ofNullable(phone);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public UserFilterOptions(String searchTerm, String username, String name, String email, String phone) {
        this(searchTerm, name, username, email, phone, null, null);
    }

    public Optional<String> getSearchTerm() {
        return searchTerm;
    }

    public Optional<String> getName() {
        return name;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public Optional<String> getEmail() {
        return email;
    }

    public Optional<String> getPhone() {
        return phone;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
