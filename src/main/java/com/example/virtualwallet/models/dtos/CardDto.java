package com.example.virtualwallet.models.dtos;

import com.example.virtualwallet.models.emus.CardType;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CardDto {


    @NotEmpty
    @Size(min = 16, max = 16, message = "Card number must be exactly 16 numbers")
    private String number;

    @NotEmpty
    @Size(min = 3, max = 3, message = "Check number must be exactly 3 numbers")
    private String check_number;
    @NotEmpty
    @Size(min = 2, max = 2)
    private String month;
    @NotEmpty
    @Size(min = 2, max = 2)
    private String year;

    @NotNull
    private CardType type;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCheck_number() {
        return check_number;
    }

    public void setCheck_number(String check_number) {
        this.check_number = check_number;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

}
