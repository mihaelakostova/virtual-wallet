package com.example.virtualwallet.models.dtos;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDto {

    @NotEmpty
    @Size(min = 2, max = 40, message = "Name must be between 2 and 40 symbols!")
    private String name;
    private String email;
    private String phone;

    @NotEmpty(message = "Username required")
    @Size(min = 2, max = 20, message = "Username must be between 2 and 20 symbols!")
    private String username;

    @NotEmpty(message = "Password required")
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$",
            message = "Password must be at least 8 characters " +
                    "and contain at least one digit, " +
                    "one lowercase letter, one uppercase letter, " +
                    "and one special character (@#$%^&+=)"
    )
    private String password;

    public UserDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

//    public String getPicture() {
//        return picture;
//    }
//
//    public void setPicture(String picture) {
//        this.picture = picture;
//    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
