package com.example.virtualwallet.models.dtos;

import javax.validation.constraints.NotEmpty;

public class LoginDto {

    @NotEmpty(message = "Username required")
//    @Size(min = 2, max = 20, message = "Username must be between 2 and 20 symbols")
    private String username;

    @NotEmpty(message = "Password required")
//    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$",
//            message = "Password must be at least 8 characters " +
//                    "and contain at least one digit, " +
//                    "one lowercase letter, one uppercase letter, " +
//                    "and one special character - @#$%^&+="
//    )
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
