package com.example.virtualwallet.models.dtos;

import javax.validation.constraints.NotEmpty;

public class RegisterDto extends UserDto {

    @NotEmpty(message = "Password confirmation required")
    private String passwordConfirm;


    public RegisterDto() {
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}