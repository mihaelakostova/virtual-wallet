package com.example.virtualwallet.models.dtos;

import javax.validation.constraints.Positive;

public class WithdrawDto {


    @Positive(message = "Amount can NOT be negative")
    private Double amount;

    public WithdrawDto() {
    }

    public Double getAmount() {
        return amount;
    }
    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
