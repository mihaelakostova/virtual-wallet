package com.example.virtualwallet.models.emus;

public enum CurrencyType {

    BGN, EUR, USD;

    @Override
    public String toString() {
        switch (this) {
            case BGN:
                return "BGN";
            case EUR:
                return "EUR";
            case USD:
                return "USD";
            default:
                return "";
        }
    }


}
