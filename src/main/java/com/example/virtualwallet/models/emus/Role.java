package com.example.virtualwallet.models.emus;

public enum Role {

    USER,
    ADMIN;

    @Override
    public String toString() {
        switch (this) {
            case ADMIN:
                return "Admin";
            case USER:
                return "User";
            default:
                return "";
        }
    }

}
