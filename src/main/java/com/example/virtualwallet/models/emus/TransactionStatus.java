package com.example.virtualwallet.models.emus;

public enum TransactionStatus {
    COMPLETED, PENDING;


    @Override
    public String toString() {
        switch (this) {
            case COMPLETED:
                return "Completed";
            case PENDING:
                return "Pending";
            default:
                return "";
        }
    }
}
