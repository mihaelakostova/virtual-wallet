package com.example.virtualwallet.models.emus;

public enum CardType {

    CREDIT, DEBIT;


    @Override
    public String toString() {
        switch (this) {
            case CREDIT:
                return "Credit";
            case DEBIT:
                return "Debit";
            default:
                return "";
        }
    }


}
