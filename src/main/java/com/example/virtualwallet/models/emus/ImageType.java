package com.example.virtualwallet.models.emus;

public enum ImageType {

    PROFILE, VERIFICATION;

    @Override
    public String toString() {
        switch (this) {
            case PROFILE:
                return "Profile";
            case VERIFICATION:
                return "Verification";
            default:
                return "";
        }
    }

}
