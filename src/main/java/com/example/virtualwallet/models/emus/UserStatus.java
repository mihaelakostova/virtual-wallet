package com.example.virtualwallet.models.emus;

public enum UserStatus {
    UNVERIFIED, VERIFIED;


    @Override
    public String toString() {
        switch (this) {
            case UNVERIFIED:
                return "Unverified";
            case VERIFIED:
                return "Verified";
            default:
                return "";
        }
    }

}
