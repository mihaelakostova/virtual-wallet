package com.example.virtualwallet.models.emus;

public enum TransactionType {
    INCOMING, OUTGOING;


    @Override
    public String toString() {
        switch (this) {
            case INCOMING:
                return "Incoming";
            case OUTGOING:
                return "Outgoing";
            default:
                return "";
        }
    }
}
