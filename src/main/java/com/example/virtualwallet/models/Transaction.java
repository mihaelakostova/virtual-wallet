package com.example.virtualwallet.models;

import com.example.virtualwallet.models.emus.TransactionStatus;
import com.example.virtualwallet.models.emus.TransactionType;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "transactions")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private int id;

    @Column(name = "amount")
    private double amount;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private Wallet sender;

    @ManyToOne
    @JoinColumn(name = "recipient_id")
    private Wallet recipient;

    @Column(name = "transaction_date")
    private LocalDateTime date;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TransactionType type;

    @Column(name = "verification_code")
    private String verificationCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private TransactionStatus status;

    public Transaction() {
    }

    public Transaction(double amount, Wallet sender, Wallet recipient, TransactionType type, TransactionStatus status) {
        this.amount = amount;
        this.sender = sender;
        this.recipient = recipient;
        this.type = type;
        this.status = status;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Wallet getSender() {
        return sender;
    }

    public void setSender(Wallet sender) {
        this.sender = sender;
    }

    public Wallet getRecipient() {
        return recipient;
    }

    public void setRecipient(Wallet recipient) {
        this.recipient = recipient;
    }

//    public String getDate() {
//        return date;
//    }

//    public void setDate() {
//        LocalDateTime currentDateTime = LocalDateTime.now();
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
//        this.date = currentDateTime.format(formatter);
//    }
    public TransactionType getType() {
        return type;
    }

    public void setType(TransactionType type) {
        this.type = type;
    }

//    public void setDate(String date) {
//        this.date = date;
//    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;

    }
}
