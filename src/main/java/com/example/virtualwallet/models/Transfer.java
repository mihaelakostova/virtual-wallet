package com.example.virtualwallet.models;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@Table(name = "transfers")
public class Transfer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transfer_id")
    private int id;
    @Column(name = "amount")
    private double amount;
    @OneToOne
    @JoinColumn(name = "sender_id")
    private Card sender;
    @OneToOne
    @JoinColumn(name = "recipient_id")
    private Wallet recipient;
    @Column(name = "transaction_date")
    private String date;

    public Transfer() {
    }

    public Transfer(int id, double amount, Card sender, Wallet recipient) {
        this.id = id;
        this.amount = amount;
        this.sender = sender;
        this.recipient = recipient;
        setDate();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Card getSender() {
        return sender;
    }

    public void setSender(Card sender) {
        this.sender = sender;
    }

    public Wallet getRecipient() {
        return recipient;
    }

    public void setRecipient(Wallet recipient) {
        this.recipient = recipient;
    }

    public String getDate() {
        return date;
    }

    public void setDate() {
        LocalDateTime currentDateTime = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        this.date = currentDateTime.format(formatter);
    }
}
