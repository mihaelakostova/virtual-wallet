package com.example.virtualwallet.models;

import com.example.virtualwallet.models.emus.CardType;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "cards")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "card_id")
    private int card_id;

    @Column(name = "number")
    private String number;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "check_number")
    private String check_number;

    @Column(name = "month")
    private String month;

    @Column(name = "year")
    private String year;

    @Enumerated(EnumType.STRING)
    private CardType type;

    @Column(name = "amount")
    private double amount;

    @Column(name = "is_blocked")
    private boolean isBlocked = false;


    public Card() {

    }

    public Card(int card_id, String number, User user, String check_number, String month, String year, CardType type, double amount) {
        this.card_id = card_id;
        this.number = number;
        this.user = user;
        this.check_number = check_number;
        this.type = type;
        this.amount = amount;
        this.month = month;
        this.year = year;
    }

    public int getCard_id() {
        return card_id;
    }

    public void setCard_id(int card_id) {
        this.card_id = card_id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCheck_number() {
        return check_number;
    }

    public void setCheck_number(String check_number) {
        this.check_number = check_number;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public CardType getType() {
        return type;
    }

    public void setType(CardType type) {
        this.type = type;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return card_id == card.card_id && Objects.equals(number, card.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(card_id, number);
    }
}
