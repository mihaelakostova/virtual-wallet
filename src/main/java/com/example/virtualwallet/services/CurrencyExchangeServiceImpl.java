package com.example.virtualwallet.services;

import com.example.virtualwallet.models.Currency;
import com.example.virtualwallet.models.ExchangeRate;
import com.example.virtualwallet.models.emus.CurrencyType;
import com.example.virtualwallet.repositories.contracts.CurrencyRepository;
import com.example.virtualwallet.repositories.contracts.ExchangeRatesRepository;
import com.example.virtualwallet.services.contracts.CurrencyExchangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {
    private final ExchangeRatesRepository exchangeRatesRepository;
    private final CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyExchangeServiceImpl(ExchangeRatesRepository exchangeRatesRepository, CurrencyRepository currencyRepository) {
        this.exchangeRatesRepository = exchangeRatesRepository;
        this.currencyRepository = currencyRepository;
    }

    @Override
    public double exchange(CurrencyType from, CurrencyType to, double amount) {
        Currency senderCurrency = currencyRepository.getByField("type", from);
        Currency recipientCurrency = currencyRepository.getByField("type", to);
        if (senderCurrency.equals(recipientCurrency)) {
            return amount;
        }
        ExchangeRate exchangeRate = exchangeRatesRepository.getByType(senderCurrency, recipientCurrency);
        return amount * exchangeRate.getRate();
    }

}
