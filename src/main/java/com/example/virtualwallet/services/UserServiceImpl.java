package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Invitation;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.emus.Role;
import com.example.virtualwallet.models.emus.UserStatus;
import com.example.virtualwallet.models.filters.UserFilterOptions;
import com.example.virtualwallet.repositories.contracts.InvitationRepository;
import com.example.virtualwallet.repositories.contracts.UserRepository;
import com.example.virtualwallet.services.contracts.EmailService;
import com.example.virtualwallet.services.contracts.RecaptchaService;
import com.example.virtualwallet.services.contracts.UserService;
import com.example.virtualwallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

@Service
//@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private static final String TOKEN_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    public static final String ONLY_ADMIN_CAN_VERIFY = "Only Admin can verify";
    private final UserRepository userRepository;
    private final InvitationRepository invitationRepository;
    private final WalletService walletService;
    private final EmailService emailService;
    private final RecaptchaService recaptchaService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, InvitationRepository invitationRepository, WalletService walletService, EmailService emailService, RecaptchaService recaptchaService) {
        this.userRepository = userRepository;
        this.invitationRepository = invitationRepository;
        this.walletService = walletService;
        this.emailService = emailService;
        this.recaptchaService = recaptchaService;
    }


    @Override
    public List<User> getAll(UserFilterOptions userFilterOptions) {
        return userRepository.getAll(userFilterOptions);
    }

    @Override
    public Page<User> getAllPaged(UserFilterOptions userFilterOptions, Pageable pageable) {
        return userRepository.getAllPaged(userFilterOptions, pageable);
    }

    @Override
    public boolean emailExists(String email) {
        return userRepository.emailExists(email);
    }

    @Override
    public boolean usernameExists(String username) {
        return userRepository.usernameExists(username);
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByField("email", email);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByField("username", username);
    }

    @Override
    public User getByPhone(String phone) {
        return userRepository.getByField("phone", phone);
    }


    @Override
    public void create(User user, String recaptchaResponse) {
        //TODO recaptcha
        recaptchaService.validateRecaptcha(recaptchaResponse);
        boolean duplicateEmailExists = userRepository.emailExists(user.getEmail());
        boolean duplicateUsernameExists = userRepository.usernameExists(user.getUsername());

        if (duplicateEmailExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        if (duplicateUsernameExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }

        String confirmationToken = generateConfirmationToken();
        user.setConfirmationToken(confirmationToken);
        user.setRole(Role.USER);
        user.setBlocked(true);
        userRepository.create(user);

        String confirmationLink = "http://localhost:8080/auth/confirm?token=" + confirmationToken;
        emailService.sendAccountConfirmationEmail(user.getEmail(), confirmationLink);
    }

    @Override
    public void createFromInvitation(User user, Invitation invitation, String recaptchaResponse) {
        try {
            create(user, recaptchaResponse);
            walletService.create(user);
            userRepository.awardUsers(user, invitation);
        } catch (DuplicateEntityException e) {
            throw new DuplicateEntityException(e.getMessage());
        }
    }

    @Override
    public void verifyAccount(String token) {
        User user = userRepository.getByField("confirmationToken", token);
        if (user == null) {
            throw new UnauthorizedOperationException("Invalid token.");
        }

        user.setUserStatus(UserStatus.VERIFIED);
        user.setConfirmationToken(null);
        userRepository.update(user);
    }

    @Override
    public void update(User user) {
        boolean duplicateExists = true;

        try {
            User existingUser = userRepository.getByField("email", user.getEmail());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }

        userRepository.update(user);
    }

    @Override
    public void blockUser(User user) {
        userRepository.blockUser(user);
    }

    @Override
    public void unblockUser(User user) {
        userRepository.unblockUser(user);
    }

    @Override
    public void delete(int id, User currentUser) {
        if (currentUser.getRole() != Role.ADMIN) {
            if (currentUser.getId() != id) {
                throw new UnauthorizedOperationException("You are not authorized to delete this user.");
            }
        }
        userRepository.delete(id);
    }

    @Override
    public void changeVerificationStatus(User user, User logUser) {
        if (logUser.getRole() != Role.ADMIN) {
            throw new UnauthorizedOperationException("Only for admin");
        }
        User userToUpdate = userRepository.getById(user.getId());
        userToUpdate.setBlocked(!userToUpdate.isBlocked());
        userRepository.update(userToUpdate);
    }

    @Override
    public User findByUsernameOrEmailOrPhone(String searchTerm) {
        return userRepository.findByUsernameOrEmailOrPhone(searchTerm);
    }

    private String generateConfirmationToken() {
        Random random = new SecureRandom();
        StringBuilder token = new StringBuilder(5);
        for (int i = 0; i < 5; i++) {
            int randomIndex = random.nextInt(TOKEN_CHARACTERS.length());
            char randomChar = TOKEN_CHARACTERS.charAt(randomIndex);
            token.append(randomChar);
        }
        return token.toString();
    }


}
