package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.Invitation;
import com.example.virtualwallet.models.User;

public interface InvitationService {
    void createInvitation(User invitingUser, String email);

    Invitation validateInvitationToken(String token);

}
