package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.filters.TransactionFilterOptions;
import com.example.virtualwallet.models.filters.UserFilterOptions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TransactionService {

    Transaction getById(int id);

    Transaction getByCode(String code);

    List<Transaction> getAll();

    Page<Transaction> getAllPaged(TransactionFilterOptions transactionFilterOptions, Pageable pageable);

    void create(Transaction transaction, User user);

    void verify(Transaction transaction, User user, String code);

    void execute(Transaction transaction);

    void cancel(Transaction currentTransaction);

    List<Transaction> getAllFromUser(User user);
}
