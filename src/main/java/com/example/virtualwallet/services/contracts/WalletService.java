package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.filters.WalletFilterOptions;

import java.util.List;

public interface WalletService {

    List<Wallet> getAll();

    List<Wallet> getAll(WalletFilterOptions walletFilterOptions);

    Wallet getById(int id);

    Wallet getByNumber(String number);

    void addUserToWallet(User logged, User user, Wallet wallet);

    void removeUserFromWallet(User logged, User user, Wallet wallet);

    void create(User user);

    void update(Wallet wallet, User user);

    void delete(int id, User user);

    void changeBlockStatus(Wallet wallet, User user);
}
