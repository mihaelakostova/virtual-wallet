package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.Image;
import com.example.virtualwallet.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ImageService {

    String uploadImage(byte[] imageDate, String username) throws IOException;

    void save(Image image);

    void delete(int id) throws IOException;

    void destroyImage(Image image, String username) throws IOException;

    Image getImageById(int id);

    List<Image> getAllBlocked();

    void changeBlockStatus(Image image, User user);


}
