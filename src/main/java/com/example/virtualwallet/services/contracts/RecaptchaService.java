package com.example.virtualwallet.services.contracts;

public interface RecaptchaService {

    void validateRecaptcha(String response);

}
