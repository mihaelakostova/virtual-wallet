package com.example.virtualwallet.services.contracts;


import com.example.virtualwallet.models.emus.CurrencyType;

public interface CurrencyExchangeService {

    double exchange(CurrencyType from, CurrencyType to, double amount);
}
