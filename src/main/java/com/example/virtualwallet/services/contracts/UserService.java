package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.Invitation;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.filters.UserFilterOptions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

    List<User> getAll(UserFilterOptions userFilterOptions);

    Page<User> getAllPaged(UserFilterOptions userFilterOptions, Pageable pageable);

    boolean emailExists(String email);

    boolean usernameExists(String username);

    User getById(int id);

    User getByEmail(String email);

    User getByUsername(String username);

    User getByPhone(String phone);

    void create(User user, String recaptchaResponse);

    void update(User user);

    void blockUser(User user);

    void unblockUser(User user);

    void delete(int id, User currentUser);

    void createFromInvitation(User user, Invitation invitation, String recaptchaResponse);

    void verifyAccount(String token);

    void changeVerificationStatus(User user, User logUser);

    User findByUsernameOrEmailOrPhone(String searchTerm);
}
