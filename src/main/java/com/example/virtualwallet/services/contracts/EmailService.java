package com.example.virtualwallet.services.contracts;

public interface EmailService {
    void sendReferralEmail(String to, String referralLink, String username);

    void sendLargeTransactionVerificationEmail(String to, String verificationCode);

    void sendAccountConfirmationEmail(String to, String confirmationLink);
}
