package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.filters.CardFilterOptions;

import java.util.List;

public interface CardService {

    Card getById(int id);

    List<Card> getByUser(User user);

    Card getByNumber(String number);

    void create(Card card, User user);

    void delete(int id, User user);

    void changeBlockStatus(Card card, User user);
}
