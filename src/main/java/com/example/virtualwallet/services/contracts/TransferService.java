package com.example.virtualwallet.services.contracts;

import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.User;

import java.util.List;

public interface TransferService {
    Transfer getById(int id);

    List<Transfer> getAll();

    void create(Transfer transfer, User user);

    void withdraw(Transfer transfer, User user);

    void topUp(Transfer transfer, User user);

}
