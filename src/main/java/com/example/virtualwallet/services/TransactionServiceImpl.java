package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.exceptions.UnverifiedUserException;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.emus.TransactionStatus;
import com.example.virtualwallet.models.emus.TransactionType;
import com.example.virtualwallet.models.filters.TransactionFilterOptions;
import com.example.virtualwallet.models.emus.UserStatus;
import com.example.virtualwallet.repositories.contracts.TransactionRepository;
import com.example.virtualwallet.repositories.contracts.WalletRepository;
import com.example.virtualwallet.services.contracts.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;
    private final WalletRepository walletRepository;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, WalletRepository walletRepository) {
        this.transactionRepository = transactionRepository;
        this.walletRepository = walletRepository;
    }

    @Override
    public Transaction getById(int id) {
        return transactionRepository.getById(id);
    }

    @Override
    public Transaction getByCode(String verificationCode) {
        return transactionRepository.getByField("verificationCode", verificationCode);
    }

    @Override
    public List<Transaction> getAll() {
        return transactionRepository.getAll();
    }

    @Override
    public Page<Transaction> getAllPaged(TransactionFilterOptions transactionFilterOptions, Pageable pageable) {
        return transactionRepository.getAllPaged(transactionFilterOptions, pageable);
    }

    @Override
    public void create(Transaction transaction, User user) {
        if (!user.getWallets().contains(transaction.getSender())) {
            throw new UnauthorizedOperationException("Sender wallet not found.");
        }
        if (user.isBlocked()) {
            throw new UnauthorizedOperationException("You are currently blocked and cannot make transactions.");
        }
        if (user.getUserStatus().equals(UserStatus.UNVERIFIED)) {
            throw new UnverifiedUserException("Please verify your e-mail by clicking the link in your e-mail inbox.");
        }
        if (transaction.getSender().getNumber().equals(transaction.getRecipient().getNumber())) {
            throw new UnauthorizedOperationException("You cannot transfer money to the same wallet");
        }
        if (!(transaction.getSender().getBalance() >= transaction.getAmount())) {
            throw new UnauthorizedOperationException("Insufficient funds.");
        }
        transaction.setDate(LocalDateTime.now());
        transactionRepository.create(transaction);

    }

    @Override
    public void verify(Transaction transaction, User user, String code) {
        if (!transaction.getSender().getCreator().getUsername().equals(user.getUsername())) {
            throw new UnauthorizedOperationException("Invalid code");
        }
        if (!transaction.getVerificationCode().equals(code)) {
            throw new UnauthorizedOperationException("Invalid code");
        }
        transactionRepository.execute(transaction);
    }

    @Override
    public void execute(Transaction transaction) {
        if (transaction.getSender().getBalance() >= transaction.getAmount()) {
            transactionRepository.execute(transaction);
        } else {
            throw new UnauthorizedOperationException("Insufficient funds.");
        }
    }

    @Override
    public void cancel(Transaction currentTransaction) {
        transactionRepository.delete(currentTransaction.getId());
    }

    @Override
    public List<Transaction> getAllFromUser(User user) {
        return transactionRepository.getAllFromUser(user);
    }

}
