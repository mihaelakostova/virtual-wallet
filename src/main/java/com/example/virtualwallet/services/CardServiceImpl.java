package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.filters.CardFilterOptions;
import com.example.virtualwallet.repositories.contracts.CardRepository;
import com.example.virtualwallet.services.contracts.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.virtualwallet.utils.constants.ONLY_ADMIN_OR_CARD_OWNER_CAN_DELETE;


@Service
public class CardServiceImpl implements CardService {

    private final CardRepository cardRepository;

    @Autowired
    public CardServiceImpl(CardRepository cardRepository) {
        this.cardRepository = cardRepository;
    }

    @Override
    public Card getById(int id) {
        return cardRepository.getById(id);
    }

    @Override
    public List<Card> getByUser(User user) {
        return cardRepository.getByUser(user);
    }

    @Override
    public Card getByNumber(String number) {
        return cardRepository.getByField("number", number);
    }


    @Override
    public void create(Card card, User user) {
        boolean duplicateExists = cardRepository.numberExists(card.getNumber());
        if (duplicateExists) {
            throw new DuplicateEntityException("Card", "Card Number", card.getNumber());
        }
        card.setUser(user);
        cardRepository.create(card);
    }


    @Override
    public void delete(int id, User user) {
        checkUserStatus(cardRepository.getById(id), user);
        cardRepository.delete(id);
    }

    @Override
    public void changeBlockStatus(Card card, User user) {
        checkUserStatus(card, user);
        Card cardUpdateStatus = cardRepository.getById(card.getCard_id());
        cardUpdateStatus.setBlocked(!cardUpdateStatus.isBlocked());
        cardRepository.update(cardUpdateStatus);
    }

    private void checkUserStatus(Card card, User user) {
        if (!(user.isAdmin() || card.getUser().getUsername().equals(user.getUsername()))) {
            throw new UnauthorizedOperationException(ONLY_ADMIN_OR_CARD_OWNER_CAN_DELETE);
        }
    }

}
