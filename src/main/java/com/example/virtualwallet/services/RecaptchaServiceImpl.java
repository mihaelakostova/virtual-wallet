package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.InvalidRecaphaException;
import com.example.virtualwallet.models.responses.RecaptchaResponse;
import com.example.virtualwallet.services.contracts.RecaptchaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.example.virtualwallet.utils.constants.*;
import static org.springframework.http.HttpMethod.POST;

@Service
public class RecaptchaServiceImpl implements RecaptchaService {

    private final String recaptchaSecret;
    private final RestTemplate restTemplate;

    @Autowired
    public RecaptchaServiceImpl(Environment environment, RestTemplate restTemplate) {
        this.recaptchaSecret = environment.getProperty(RECAPTCHA_SECRET_ATT);
        this.restTemplate = restTemplate;
    }

    @Override
    public void validateRecaptcha(String response) {
        RecaptchaResponse recaptchaResponse = getResponse(response);
        if (!recaptchaResponse.isSuccess()) {
            throw new InvalidRecaphaException("You are a robot");
        }
    }

    private RecaptchaResponse getResponse(String response) {
        final String recaptchaUrlAndParams = RECAPTCHA_API + QUERY_SECRET + recaptchaSecret + AND_RESPONSE + response;
        ResponseEntity<RecaptchaResponse> exchange = restTemplate.exchange(recaptchaUrlAndParams,
                POST, null, RecaptchaResponse.class);
        return exchange.getBody();
    }

}
