package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.InvalidTokenException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Invitation;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.repositories.contracts.InvitationRepository;
import com.example.virtualwallet.repositories.contracts.UserRepository;
import com.example.virtualwallet.services.contracts.EmailService;
import com.example.virtualwallet.services.contracts.InvitationService;
import com.example.virtualwallet.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.time.LocalDateTime;

@Service
public class InvitationServiceImpl implements InvitationService {
    private final InvitationRepository invitationRepository;
    private final EmailService emailService;
    private final UserService userService;
    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    @Autowired
    public InvitationServiceImpl(InvitationRepository invitationRepository, EmailService emailService, UserService userService) {
        this.invitationRepository = invitationRepository;
        this.emailService = emailService;
        this.userService = userService;
    }

    @Override
    public void createInvitation(User invitingUser, String email) {
        if (userService.emailExists(email)) {
            throw new DuplicateEntityException("User", "e-mail", email);
        }
        if (invitingUser.getReferals() <= 0) {
            throw new UnauthorizedOperationException("You have reached the limit of friends you can refer.");
        }
        String token = generateToken();

        LocalDateTime expirationDate = LocalDateTime.now().plusHours(48);

        Invitation invitation = new Invitation();
        invitation.setInvitingUser(invitingUser);
        invitation.setEmail(email);
        invitation.setToken(token);
        invitation.setExpirationDate(expirationDate);
        invitation.setValid(true);

        String referralLink = "http://localhost:8080/auth/referral?token=" + token;

        invitationRepository.create(invitation);
        emailService.sendReferralEmail(email, referralLink, invitingUser.getUsername());
    }

    @Override
    public Invitation validateInvitationToken(String token) {
        Invitation invitation = invitationRepository.getByField("token", token);

        if (invitation == null) {
            throw new InvalidTokenException("Invalid invitation token");
        }
        if (!invitation.isValid()) {
            throw new InvalidTokenException("The referral link has already been used.");
        }

        if (invitation.getExpirationDate().isBefore(LocalDateTime.now())) {
            throw new InvalidTokenException("Invitation token has expired");
        }
        return invitation;
    }

    public static String generateToken() {
        SecureRandom random = new SecureRandom();
        StringBuilder token = new StringBuilder(6);

        for (int i = 0; i < 6; i++) {
            int randomIndex = random.nextInt(ALPHABET.length());
            char randomChar = ALPHABET.charAt(randomIndex);
            token.append(randomChar);
        }

        return token.toString();
    }
}
