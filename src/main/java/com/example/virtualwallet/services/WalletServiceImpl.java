package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.emus.CurrencyType;
import com.example.virtualwallet.models.filters.WalletFilterOptions;
import com.example.virtualwallet.repositories.contracts.UserRepository;
import com.example.virtualwallet.repositories.contracts.WalletRepository;
import com.example.virtualwallet.services.contracts.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.virtualwallet.utils.constants.ONLY_ADMIN_OR_WALLET_OWNER_CAN_DELETE;
import static com.example.virtualwallet.utils.constants.ONLY_USER_WHO_ARE_IN_WALLET_CAN_BE_REMOVE;

@Service
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;
    private final UserRepository userRepository;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository, UserRepository userRepository) {
        this.walletRepository = walletRepository;
        this.userRepository = userRepository;
    }

    @Override
    public Wallet getById(int id) {
        return walletRepository.getById(id);
    }

    @Override
    public Wallet getByNumber(String number) {
        return walletRepository.getByField("number", number);
    }

    @Override
    public List<Wallet> getAll() {
        return walletRepository.getAll();
    }

    @Override
    public List<Wallet> getAll(WalletFilterOptions walletFilterOptions) {
        return walletRepository.getAll(walletFilterOptions);
    }

    @Override
    public void addUserToWallet(User logged, User user, Wallet wallet) {

        checkUserStatus(wallet, logged);
        wallet.getHolders().add(user);
        user.getWallets().add(wallet);
        walletRepository.update(wallet);
        userRepository.update(user);
    }

    @Override
    public void removeUserFromWallet(User logged, User user, Wallet wallet) {
        checkUserStatus(wallet, logged);
        checkContainsUser(wallet, user);
        wallet.getHolders().remove(user);
        user.getWallets().remove(wallet);
        walletRepository.update(wallet);
        userRepository.update(user);
    }

    @Override
    public void create(User user) {
        User creator = userRepository.getById(user.getId());
        Wallet wallet = new Wallet();
        wallet.setBlocked(false);
        wallet.setDeleted(false);
        wallet.setNumber();
        wallet.setBalance(0.00);
        wallet.setCreator(creator);
        wallet.setCurrency(CurrencyType.BGN);
        walletRepository.create(wallet);
        wallet.getHolders().add(creator);
        creator.getWallets().add(wallet);
        walletRepository.update(wallet);
        userRepository.update(creator);
    }


    @Override
    public void update(Wallet wallet, User user) {
        checkContainsUser(wallet, user);
        walletRepository.update(wallet);
    }

    @Override
    public void delete(int id, User user) {
        User logUser = userRepository.getById(user.getId());
        Wallet walletToDelete = walletRepository.getById(id);
        checkUserStatus(walletToDelete, logUser);
        user.getWallets().remove(walletToDelete);
        userRepository.update(user);
        walletToDelete.setDeleted(true);
        walletRepository.update(walletToDelete);
    }

    @Override
    public void changeBlockStatus(Wallet wallet, User user) {
        checkUserStatus(wallet, user);
        Wallet walletToUpdateStatus = walletRepository.getById(wallet.getWallet_id());
        walletToUpdateStatus.setBlocked(!walletToUpdateStatus.isBlocked());
        walletRepository.update(walletToUpdateStatus);
    }

    private void checkContainsUser(Wallet wallet, User user) {
        if (!wallet.getHolders().contains(user)) {
            throw new UnauthorizedOperationException(ONLY_USER_WHO_ARE_IN_WALLET_CAN_BE_REMOVE);
        }
    }

    private void checkUserStatus(Wallet wallet, User logUser) {
        if (!wallet.getCreator().getUsername().equals(logUser.getUsername())) {
            throw new UnauthorizedOperationException(ONLY_ADMIN_OR_WALLET_OWNER_CAN_DELETE);
        }
    }

}
