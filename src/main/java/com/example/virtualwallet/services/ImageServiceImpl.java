package com.example.virtualwallet.services;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.example.virtualwallet.exceptions.FileStorageException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.helpers.ImageUtility;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.Image;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.emus.ImageType;
import com.example.virtualwallet.models.emus.Role;
import com.example.virtualwallet.repositories.contracts.ImageRepository;
import com.example.virtualwallet.repositories.contracts.UserRepository;
import com.example.virtualwallet.services.contracts.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.example.virtualwallet.utils.constants.ONLY_ADMIN_OR_CARD_OWNER_CAN_DELETE;

@Service
public class ImageServiceImpl implements ImageService {
    private final Cloudinary cloudinary;
    private final ImageRepository imageRepository;

    @Autowired
    public ImageServiceImpl(Cloudinary cloudinary, ImageRepository imageRepository) {
        this.cloudinary = cloudinary;
        this.imageRepository = imageRepository;
    }

    @Override
    public String uploadImage(byte[] imageDate, String username) throws IOException {

        Map uploadResult = cloudinary.uploader().upload(imageDate, ObjectUtils.asMap(
                "public_id", username));

        return (String) uploadResult.get("secure_url");
    }

    @Override
    public void save(Image image) {
        this.imageRepository.save(image);
    }

    @Override
    public void delete(int id) throws IOException {
        this.imageRepository.delete(id);
    }

    @Override
    public void destroyImage(Image image, String username) throws IOException {
        cloudinary.uploader().destroy(username, null);
    }

    @Override
    public Image getImageById(int id) {
        return imageRepository.getImageById(id);
    }

    @Override
    public List<Image> getAllBlocked() {
        return imageRepository.getAllBlock();
    }

    @Override
    public void changeBlockStatus(Image image, User user) {
        if (!user.getRole().equals(Role.ADMIN)) {
            throw new UnauthorizedOperationException(ONLY_ADMIN_OR_CARD_OWNER_CAN_DELETE);
        }
        Image imageStatus = imageRepository.getById(image.getId());

        imageStatus.setBlocked(!imageStatus.isBlocked());

        imageRepository.update(imageStatus);
    }

}

