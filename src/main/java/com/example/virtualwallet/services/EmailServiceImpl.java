package com.example.virtualwallet.services;

import com.example.virtualwallet.services.contracts.EmailService;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender javaMailSender;

    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Async
    public void sendEmail(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply.vwallet@gmail.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        javaMailSender.send(message);
    }

    public void sendReferralEmail(String to, String referralLink, String username) {
        String subject = "Invitation to Join VWallet";
        String text = String.format("You have been invited by %s to join our virtual wallet app! " +
                "Click on the following link to register: %s", username, referralLink);

        sendEmail(to, subject, text);
    }

    public void sendLargeTransactionVerificationEmail(String to, String verificationCode) {
        String subject = "Large Transaction Verification";
        String text = "Please confirm the transaction by using this verification code: " + verificationCode;
        sendEmail(to, subject, text);
    }

    public void sendAccountConfirmationEmail(String to, String confirmationLink) {
        String subject = "Account Confirmation";
        String message = "Thank you for registering for VWallet!\n"
                + "To activate your account, please click the link below:\n"
                + confirmationLink;

        sendEmail(to, subject, message);
    }
}
