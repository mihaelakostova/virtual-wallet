package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.repositories.contracts.CardRepository;
import com.example.virtualwallet.repositories.contracts.TransferRepository;
import com.example.virtualwallet.repositories.contracts.WalletRepository;
import com.example.virtualwallet.services.contracts.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransferServiceImpl implements TransferService {
    private final TransferRepository transferRepository;
    private final WalletRepository walletRepository;
    private final CardRepository cardRepository;

    @Autowired
    public TransferServiceImpl(TransferRepository transferRepository, WalletRepository walletRepository, CardRepository cardRepository) {
        this.transferRepository = transferRepository;
        this.walletRepository = walletRepository;
        this.cardRepository = cardRepository;
    }

    @Override
    public Transfer getById(int id) {
        return transferRepository.getById(id);
    }

    @Override
    public List<Transfer> getAll() {
        return transferRepository.getAll();
    }

    @Override
    public void create(Transfer transfer, User user) {
        if (!transfer.getSender().getUser().getUsername().equals(user.getUsername())) {
            throw new UnauthorizedOperationException("The selected card does not belong to the user.");
        }
        if (user.isBlocked()) {
            throw new UnauthorizedOperationException("You are currently blocked and cannot make transfers.");
        }
        if ((transfer.getSender().getAmount() < transfer.getAmount())) {
            throw new UnauthorizedOperationException("Insufficient funds.");
        }
        transfer.getSender().setAmount(transfer.getSender().getAmount() - transfer.getAmount());
        transfer.getRecipient().setBalance(transfer.getRecipient().getBalance() + transfer.getAmount());
        cardRepository.update(transfer.getSender());
        walletRepository.update(transfer.getRecipient());
        transferRepository.create(transfer);

    }

    @Override
    public void withdraw(Transfer transfer, User user) {
        checkUserStatus(transfer, user);

        transfer.getSender().setAmount(transfer.getSender().getAmount() - transfer.getAmount());
        walletRepository.update(transfer.getRecipient());

    }

    @Override
    public void topUp(Transfer transfer, User user) {
        checkUserStatus(transfer, user);

        transfer.getRecipient().setBalance(transfer.getRecipient().getBalance() + transfer.getAmount());
        walletRepository.update(transfer.getRecipient());
    }


    private void checkUserStatus(Transfer transfer, User logUser) {
        if (!transfer.getSender().getUser().getUsername().equals(logUser.getUsername())) {
            throw new UnauthorizedOperationException("The selected card does not belong to the user.");
        }
        if (logUser.isBlocked()) {
            throw new UnauthorizedOperationException("You are currently blocked and cannot make transfers.");
        }
    }
}
