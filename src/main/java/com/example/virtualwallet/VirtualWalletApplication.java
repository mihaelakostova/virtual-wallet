package com.example.virtualwallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;

@SpringBootApplication
public class VirtualWalletApplication {

    public static void main(String[] args) {

        SpringApplication.run(VirtualWalletApplication.class, args);
    }
}
