package com.example.virtualwallet.repositories;

import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.filters.WalletFilterOptions;
import com.example.virtualwallet.repositories.contracts.WalletRepository;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class WalletRepositoryImpl extends AbstractCRUDRepository<Wallet> implements WalletRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public WalletRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Wallet.class);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Wallet> getAll(WalletFilterOptions walletFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            walletFilterOptions.getNumber().ifPresent(value -> {
                filters.add("number like:number");
                params.put("number", String.format("%%%s%%", value));
            });

            walletFilterOptions.getMinBalance().ifPresent(value -> {
                filters.add("balance >= : balance");
                params.put("balance", value);
            });

            walletFilterOptions.getMaxBalance().ifPresent(value -> {
                filters.add("balance <= : balance");
                params.put("balance", value);
            });

            StringBuilder builderQuery = new StringBuilder("from Wallet");
            if (!filters.isEmpty()) {
                builderQuery
                        .append(" where ")
                        .append(String.join(" and ", filters));
            }
            builderQuery.append(generateOrderBy(walletFilterOptions));

            Query<Wallet> query = session.createQuery(builderQuery.toString(), Wallet.class);
            query.setProperties(params);
            return query.list();
        }
    }

    private String generateOrderBy(WalletFilterOptions walletFilterOptions) {
        if (walletFilterOptions.getSortBy().isEmpty()) {
            return "";
        }

        String orderBy = "";
        switch (walletFilterOptions.getSortBy().get()) {
            case "number":
                orderBy = "number";
                break;
            case "balance":
                orderBy = "balance";
                break;
            default:
                orderBy = "wallet_id";
        }

        orderBy = String.format(" order by %s", orderBy);

        if (walletFilterOptions.getSortOrder().isPresent() &&
                walletFilterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy = String.format("%s desc", orderBy);
        }
        return orderBy;
    }

    @Override
    public Wallet getByUserId(int id) {
        try (Session session = sessionFactory.openSession()) {
            String hql = "FROM Wallet uw WHERE uw.creator.id = :id";
            return session.createQuery(hql, Wallet.class)
                    .setParameter("id", id)
                    .setMaxResults(1)
                    .uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
