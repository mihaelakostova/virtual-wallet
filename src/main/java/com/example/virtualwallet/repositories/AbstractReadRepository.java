package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.repositories.contracts.BaseReadRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public abstract class AbstractReadRepository<T> implements BaseReadRepository<T> {

    protected final SessionFactory sessionFactory;
    protected final Class<T> tClass;

    @Autowired
    protected AbstractReadRepository(SessionFactory sessionFactory, Class<T> tClass) {
        this.sessionFactory = sessionFactory;
        this.tClass = tClass;
    }

    @Override
    public List<T> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery(String.format("from %s", tClass.getSimpleName()), tClass).list();
        }
    }

    @Override
    public T getById(int id) {
        return getByField("id", id);
    }

    @Override
    public <V> T getByField(String name, V value) {
        String notFoundMessage = String.format("%s with %s %s not found", tClass.getSimpleName(), name, value);

        try (Session session = sessionFactory.openSession()) {
            List<T> resultList = session
                    .createQuery(String.format("from %s where %s = :value", tClass.getSimpleName(), name), tClass)
                    .setParameter("value", value)
                    .list();
            if (!resultList.isEmpty()) {
                return resultList.get(0);
            } else
                throw new EntityNotFoundException(notFoundMessage);
        }
    }
}
