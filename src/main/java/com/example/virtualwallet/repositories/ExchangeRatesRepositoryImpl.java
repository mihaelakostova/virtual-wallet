package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.Currency;
import com.example.virtualwallet.models.ExchangeRate;
import com.example.virtualwallet.repositories.contracts.CurrencyRepository;
import com.example.virtualwallet.repositories.contracts.ExchangeRatesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ExchangeRatesRepositoryImpl extends AbstractCRUDRepository<ExchangeRate> implements ExchangeRatesRepository {

    private final SessionFactory sessionFactory;
    private final CurrencyRepository currencyRepository;

    @Autowired
    public ExchangeRatesRepositoryImpl(SessionFactory sessionFactory, CurrencyRepository currencyRepository) {
        super(sessionFactory, ExchangeRate.class);
        this.currencyRepository = currencyRepository;
        this.sessionFactory = sessionFactory;
    }


    @Override
    public ExchangeRate getByType(Currency sender, Currency recipient) {
        try (Session session = sessionFactory.openSession()) {
            int yourFromCurrencyId = sender.getId();
            int yourToCurrencyId = recipient.getId();

            String hql = "FROM ExchangeRate er " +
                    "WHERE er.fromCurrency = :fromCurrency " +
                    "AND er.toCurrency = :toCurrency";

            Query<ExchangeRate> query = session.createQuery(hql, ExchangeRate.class)
                    .setParameter("fromCurrency", sender)
                    .setParameter("toCurrency", recipient);

            List<ExchangeRate> exchangeRates = query.getResultList();

            if (exchangeRates.size() == 0) {
                throw new EntityNotFoundException("Exchange rate", "", "");
            } else {
                return exchangeRates.stream().filter(exchangeRate -> exchangeRate.getToCurrency().getId() == recipient.getId()).findFirst().get();
            }
        }
    }


}

