package com.example.virtualwallet.repositories;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.filters.CardFilterOptions;
import com.example.virtualwallet.repositories.contracts.CardRepository;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Repository
public class CardRepositoryImpl extends AbstractCRUDRepository<Card> implements CardRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    protected CardRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Card.class);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Card> getAll(CardFilterOptions cardFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            cardFilterOptions.getNumber().ifPresent(value -> {
                filters.add("number like:number");
                params.put("number", String.format("%%%s%%", value));
            });

            cardFilterOptions.getCheck_number().ifPresent(value -> {
                filters.add("check_number like:check_number");
                params.put("check_number", String.format("%%%s%%", value));
            });


            StringBuilder builderQuery = new StringBuilder("from Card ");
            if (!filters.isEmpty()) {
                builderQuery.append(" where ")
                        .append(String.join(" and ", filters));
            }
            builderQuery.append(generateOrderBy(cardFilterOptions));
            Query<Card> query = session.createQuery(builderQuery.toString(), Card.class);
            query.setProperties(params);
            return query.list();
        }
    }

    @Override
    public List<Card> getByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Card> query = session.createQuery("select c from Card c where c.user = :user", Card.class);
            query.setParameter("user", user);
            return query.list();
        }
    }

    @Override
    public boolean numberExists(String number) {
        try (Session session = sessionFactory.openSession()) {
            List<Card> users = session.createQuery("from Card where number = :number", Card.class)
                    .setParameter("number", number)
                    .list();
            return !users.isEmpty();
        }
    }

    private String generateOrderBy(CardFilterOptions cardFilterOptions) {
        if (cardFilterOptions.getSortBy().isEmpty()) {
            return "";
        }

        String orderBy = "";
        switch (cardFilterOptions.getSortBy().get()) {
            case "number":
                orderBy = "number";
                break;
            case "check_number":
                orderBy = "check_number";
                break;
            default:
                orderBy = "card_id";
        }

        orderBy = String.format(" order by %s", orderBy);

        if (cardFilterOptions.getOrderBy().isPresent() &&
                cardFilterOptions.getOrderBy().get().equalsIgnoreCase("desc")) {
            orderBy = String.format("%s desc", orderBy);
        }
        return orderBy;
    }

}
