package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.Currency;
import com.example.virtualwallet.models.emus.CurrencyType;
import com.example.virtualwallet.repositories.contracts.CurrencyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CurrencyRepositoryImpl extends AbstractCRUDRepository<Currency> implements CurrencyRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CurrencyRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Currency.class);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Currency getByType(CurrencyType type) {
        try (Session session = sessionFactory.openSession()) {
            Query<Currency> query = session.createQuery("from Currency where type = :type ", Currency.class);
            query.setParameter("type", type);

            if (query.list().size() == 0) {
                throw new EntityNotFoundException("Currency", "type", type.toString());
            } else {
                return query.list().get(0);
            }
        }
    }


}
