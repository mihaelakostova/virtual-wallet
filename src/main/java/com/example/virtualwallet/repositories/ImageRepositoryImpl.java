package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.Image;
import com.example.virtualwallet.repositories.contracts.ImageRepository;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class ImageRepositoryImpl extends AbstractCRUDRepository<Image> implements ImageRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ImageRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Image.class);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Image image) {
        try(Session session = sessionFactory.openSession()){
            session.beginTransaction();
            session.save(image);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Image> getAllBlock() {
        try (Session session = sessionFactory.openSession()) {
            Query<Image> query = session.createQuery(
                    "SELECT i FROM Image i WHERE i.isBlocked = :true", Image.class);
            query.setParameter("true", true);
            return query.list();
        }
    }

    @Override
    public Image getImageById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Image> query = session.createQuery(
                    "SELECT i FROM Image i WHERE i.id = :id", Image.class);
            query.setParameter("id", id);

            Image image = query.uniqueResult();
            if (image == null) {
                throw new EntityNotFoundException("Image", "with ID", String.valueOf(id));
            }
            return image;
        }
    }










}
