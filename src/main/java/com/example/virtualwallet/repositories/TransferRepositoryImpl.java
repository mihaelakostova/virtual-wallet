package com.example.virtualwallet.repositories;

import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.repositories.contracts.TransactionRepository;
import com.example.virtualwallet.repositories.contracts.TransferRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TransferRepositoryImpl extends AbstractCRUDRepository<Transfer> implements TransferRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    protected TransferRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Transfer.class);
        this.sessionFactory = sessionFactory;
    }
}
