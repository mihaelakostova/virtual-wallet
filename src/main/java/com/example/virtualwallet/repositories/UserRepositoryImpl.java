package com.example.virtualwallet.repositories;

import com.example.virtualwallet.exceptions.EntityNotFoundException;
import com.example.virtualwallet.models.Invitation;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.filters.UserFilterOptions;
import com.example.virtualwallet.repositories.contracts.UserRepository;
import com.example.virtualwallet.repositories.contracts.WalletRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {

    private final SessionFactory sessionFactory;
    private final WalletRepository walletRepository;

    @Autowired
    protected UserRepositoryImpl(SessionFactory sessionFactory, WalletRepository walletRepository) {
        super(sessionFactory, User.class);
        this.sessionFactory = sessionFactory;
        this.walletRepository = walletRepository;
    }

    @Override
    public List<User> getAll(UserFilterOptions userFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder(" from User ");
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            userFilterOptions.getSearchTerm().ifPresent(value -> {
                filters.add(" lower(username) like lower(:searchTerm) or lower(name) like lower(:searchTerm) or lower(email) like lower(:searchTerm) or phone like :searchTerm ");
                params.put("searchTerm", "%" + value.toLowerCase() + "%");
            });

            userFilterOptions.getName().ifPresent(value -> {
                filters.add(" lower(name) like lower(:name) ");
                params.put("name", "%" + value.toLowerCase() + "%");
            });

            userFilterOptions.getEmail().ifPresent(value -> {
                filters.add(" lower(email) like lower(:email) ");
                params.put("email", "%" + value.toLowerCase() + "%");
            });

            userFilterOptions.getPhone().ifPresent(value -> {
                filters.add(" phone like :phone ");
                params.put("phone", "%" + value + "%");
            });

            if (!filters.isEmpty()) {
                queryString.append(" where ").append(String.join(" or ", filters));
            }

            queryString.append(generateOrderBy(userFilterOptions));

            Query<User> query = session.createQuery(queryString.toString(), User.class);
            query.setProperties(params);
            return query.list();
        }
    }

    @Override
    public Page<User> getAllPaged(UserFilterOptions userFilterOptions, org.springframework.data.domain.Pageable pageable) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("SELECT u FROM User u");
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            userFilterOptions.getSearchTerm().ifPresent(value -> {
                filters.add(" lower(username) like lower(:searchTerm) or lower(name) like lower(:searchTerm) or lower(email) like lower(:searchTerm) or phone like :searchTerm");
                params.put("searchTerm", "%" + value.toLowerCase() + "%");
            });

            userFilterOptions.getName().ifPresent(value -> {
                filters.add(" lower(name) like lower(:name) ");
                params.put("name", "%" + value.toLowerCase() + "%");
            });

            userFilterOptions.getEmail().ifPresent(value -> {
                filters.add(" lower(email) like lower(:email) ");
                params.put("email", "%" + value.toLowerCase() + "%");
            });

            userFilterOptions.getPhone().ifPresent(value -> {
                filters.add(" phone like :phone ");
                params.put("phone", "%" + value + "%");
            });

            if (!filters.isEmpty()) {
                queryString.append(" where ").append(String.join(" or ", filters));
            }

            queryString.append(generateOrderBy(userFilterOptions));

            Query<User> query = session.createQuery(queryString.toString(), User.class);
            query.setProperties(params);

            int totalResults = query.getResultList().size();
            query.setFirstResult((int) pageable.getOffset());
            query.setMaxResults(pageable.getPageSize());

            List<User> usersList = query.list();

            return new PageImpl<>(usersList, pageable, totalResults);
        }
    }

    @Override
    public void blockUser(User user) {
        user.setBlocked(true);
        update(user);
    }

    @Override
    public void unblockUser(User user) {
        user.setBlocked(false);
        update(user);
    }

    @Override
    public boolean emailExists(String email) {
        try (Session session = sessionFactory.openSession()) {
            List<User> userEntities = session.createQuery("from User where email = :email", User.class)
                    .setParameter("email", email)
                    .list();
            return !userEntities.isEmpty();
        }
    }

    @Override
    public boolean usernameExists(String username) {
        try (Session session = sessionFactory.openSession()) {
            List<User> userEntities = session.createQuery("from User where username = :username", User.class)
                    .setParameter("username", username)
                    .list();
            return !userEntities.isEmpty();
        }
    }

    @Override
    public User findByUsernameOrEmailOrPhone(String searchTerm) {
        try (Session session = sessionFactory.openSession()) {
            String sqlQuery = "SELECT * FROM users " +
                    "WHERE lower(username) = lower(:searchTerm) " +
                    "OR lower(email) = lower(:searchTerm) " +
                    "OR phone = :searchTerm";

            User user = (User) session.createSQLQuery(sqlQuery)
                    .addEntity(User.class)
                    .setParameter("searchTerm", searchTerm)
                    .uniqueResult();

            if (user == null) {
                throw new EntityNotFoundException(String.format("%s does not exists", searchTerm));
            }
            return user;
        }
    }

    @Override
    public void awardUsers(User user, Invitation invitation) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();


            User invitingUser = invitation.getInvitingUser();
            Wallet invitingUserWallet = walletRepository.getByUserId(invitation.getInvitingUser().getId());
            invitingUserWallet.setBalance(invitingUserWallet.getBalance() + 50);
            invitingUser.setReferals(invitingUser.getReferals() - 1);
            session.update(invitingUser);
            session.update(invitingUserWallet);

            Wallet newUserWallet = walletRepository.getByUserId(user.getId());
            newUserWallet.getHolders().add(user);
            user.getWallets().add(newUserWallet);
            newUserWallet.setBalance(newUserWallet.getBalance() + 50);
            invitation.setValid(false);
            session.update(user);
            session.update(newUserWallet);

            session.update(invitation);

            session.getTransaction().commit();
        }

    }

    private String generateOrderBy(UserFilterOptions userFilterOptions) {
        StringBuilder orderBy = new StringBuilder(" order by ");

        if (userFilterOptions.getSortBy().isEmpty()) {
            return "";
        }

        switch (userFilterOptions.getSortBy().toString()) {
            case "username":
                orderBy.append("username");
                break;
            case "name":
                orderBy.append("name");
                break;
            case "email":
                orderBy.append("email");
                break;
            case "phone":
                orderBy.append("phone");
                break;
            default:
                orderBy.append("id");
        }

        if (userFilterOptions.getSortOrder().isPresent()
                && containsIgnoreCase(userFilterOptions.getSortOrder().get(), "desc")) {
            orderBy.append(" desc ");
        }

        return orderBy.toString();
    }

    private static boolean containsIgnoreCase(String value, String sequence) {
        return value.toLowerCase().contains(sequence.toLowerCase());
    }

}
