package com.example.virtualwallet.repositories;

import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.emus.TransactionStatus;
import com.example.virtualwallet.models.filters.TransactionFilterOptions;
import com.example.virtualwallet.models.emus.TransactionType;
import com.example.virtualwallet.repositories.contracts.TransactionRepository;
import org.hibernate.Session;
import com.example.virtualwallet.repositories.contracts.WalletRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.*;

@Repository
public class TransactionRepositoryImpl extends AbstractCRUDRepository<Transaction> implements TransactionRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    protected TransactionRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Transaction.class);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Page<Transaction> getAllPaged(TransactionFilterOptions transactionFilterOptions, org.springframework.data.domain.Pageable pageable) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("SELECT t FROM Transaction t");
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            transactionFilterOptions.getMinAmount().ifPresent(value -> {
                filters.add(" amount >=: minAmount ");
                params.put("minAmount", value);
            });

            transactionFilterOptions.getMaxAmount().ifPresent(value -> {
                filters.add(" amount <=: maxAmount ");
                params.put("maxAmount", value);
            });

            transactionFilterOptions.getMinDate().ifPresent(value -> {
                filters.add(" date >=: minDate ");
                params.put("minDate", value);
            });

            transactionFilterOptions.getMaxDate().ifPresent(value -> {
                filters.add(" date <=: maxDate ");
                params.put("maxDate", value);
            });

            transactionFilterOptions.getType().ifPresent(value -> {
                filters.add(" t.type = :type ");
                params.put("type", value);
            });

            transactionFilterOptions.getStatus().ifPresent(value -> {
                filters.add(" t.status = :status ");
                params.put("status", value);
            });

            transactionFilterOptions.getSender().ifPresent(value -> {
                filters.add(" t.sender.creator.id = :senderId ");
                params.put("senderId", value);
            });

            transactionFilterOptions.getRecipient().ifPresent(value -> {
                filters.add(" t.recipient.creator.id = :recipientId ");
                params.put("recipientId", value);
            });

            if (!filters.isEmpty()) {
                queryString.append(" where ").append(String.join(" or ", filters));
            }


            queryString.append(generateOrderBy(transactionFilterOptions));

            Query<Transaction> query = session.createQuery(queryString.toString(), Transaction.class);
            query.setProperties(params);

            int totalResults = query.getResultList().size();
            query.setFirstResult((int) pageable.getOffset());
            query.setMaxResults(pageable.getPageSize());

            List<Transaction> transactionsList = query.list();

            return new PageImpl<>(transactionsList, pageable, totalResults);
        }
    }

    private String generateOrderBy(TransactionFilterOptions transactionFilterOptions) {
        if (transactionFilterOptions.getSortBy().isEmpty()) {
            return "";
        }

        StringBuilder orderBy = new StringBuilder(" ORDER BY ");

        switch (transactionFilterOptions.getSortBy().toString()) {
            case "amount":
                orderBy.append("t.amount");
                break;
            case "transaction_date":
                orderBy.append("t.date");
                break;
//            case "status":
//                orderBy.append("t.status");
//                break;
            default:
                orderBy.append("t.id");
        }

        if (transactionFilterOptions.getSortOrder().isPresent() &&
                containsIgnoreCase(transactionFilterOptions.getSortOrder().get(), "desc")) {
            orderBy.append(" DESC");
        } else {
            orderBy.append(" ASC");
        }

        return orderBy.toString();
    }

    private static boolean containsIgnoreCase(String value, String sequence) {
        return value.toLowerCase().contains(sequence.toLowerCase());
    }

    @Override
    public void execute(Transaction transaction) {
        try (Session session = sessionFactory.openSession()) {

            session.beginTransaction();

            transaction.getSender().setBalance(transaction.getSender().getBalance() - transaction.getAmount());

            transaction.setStatus(TransactionStatus.COMPLETED);

            session.update(transaction.getSender());
            session.update(transaction);

            Transaction transactionIn = new Transaction(transaction.getAmount(),
                    transaction.getSender(),
                    transaction.getRecipient(),
                    TransactionType.INCOMING,
                    TransactionStatus.COMPLETED);

            transaction.getRecipient().setBalance(transaction.getRecipient().getBalance() + transaction.getAmount());

            session.update(transaction.getRecipient());
            session.save(transactionIn);

            session.getTransaction().commit();
        }
    }

    @Override
    public List<Transaction> getAllFromUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            String hql = "FROM Transaction t WHERE " +
                    "((t.sender.creator = :user AND t.type = 'OUTGOING') OR " + // Outgoing transactions
                    "(t.recipient.creator = :user AND t.type = 'INCOMING' AND t.status <> 'PENDING')) " + // Incoming transactions
                    "ORDER BY t.date DESC";
            return session.createQuery(hql, Transaction.class)
                    .setParameter("user", user)
                    .getResultList();
        }
    }
}
