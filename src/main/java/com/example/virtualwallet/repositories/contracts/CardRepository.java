package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.filters.CardFilterOptions;

import java.util.List;

public interface CardRepository extends BaseCRUDRepository<Card> {

    List<Card> getAll(CardFilterOptions cardFilterOptions);

    List<Card> getByUser(User user);

    boolean numberExists(String number);

}
