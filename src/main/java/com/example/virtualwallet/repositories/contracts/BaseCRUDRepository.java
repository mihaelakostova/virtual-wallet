package com.example.virtualwallet.repositories.contracts;

public interface BaseCRUDRepository<T> extends BaseReadRepository<T> {

    void create(T entity);

    void update(T entity);

    void delete(int id);

}
