package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Currency;
import com.example.virtualwallet.models.emus.CurrencyType;

public interface CurrencyRepository extends BaseCRUDRepository<Currency> {

    Currency getByType(CurrencyType type);

}
