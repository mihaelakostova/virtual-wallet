package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.Image;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.emus.ImageType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ImageRepository extends BaseCRUDRepository<Image> {

    void save(Image image);

    List<Image> getAllBlock();

    Image getImageById(int id);


}
