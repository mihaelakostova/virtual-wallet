package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Invitation;

public interface InvitationRepository extends BaseCRUDRepository<Invitation>{
}
