package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.filters.TransactionFilterOptions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TransactionRepository extends BaseCRUDRepository<Transaction> {

    Page<Transaction> getAllPaged(TransactionFilterOptions transactionFilterOptions, Pageable pageable);

    void execute(Transaction transaction);

    List<Transaction> getAllFromUser(User user);
}
