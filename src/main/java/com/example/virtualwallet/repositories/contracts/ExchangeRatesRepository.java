package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.Currency;
import com.example.virtualwallet.models.ExchangeRate;

import java.util.List;

public interface ExchangeRatesRepository extends BaseCRUDRepository<ExchangeRate> {

    List<ExchangeRate> getAll();

    ExchangeRate getByType(Currency sender, Currency recipient);

}
