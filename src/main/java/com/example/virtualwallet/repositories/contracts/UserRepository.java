package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Invitation;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.filters.UserFilterOptions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserRepository extends BaseCRUDRepository<User> {

    List<User> getAll(UserFilterOptions userFilterOptions);

    Page<User> getAllPaged(UserFilterOptions userFilterOptions, Pageable pageable);

    void blockUser(User user);

    void unblockUser(User user);

    boolean emailExists(String email);

    boolean usernameExists(String username);

//    Page<User> getAllPaged(Pageable pageable);

    void awardUsers(User user, Invitation invitation);

    User findByUsernameOrEmailOrPhone(String searchTerm);
}
