package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.Transfer;

public interface TransferRepository extends BaseCRUDRepository<Transfer> {
}
