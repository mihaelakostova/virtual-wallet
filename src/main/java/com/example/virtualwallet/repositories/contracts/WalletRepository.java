package com.example.virtualwallet.repositories.contracts;

import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.filters.WalletFilterOptions;

import java.util.List;

public interface WalletRepository extends BaseCRUDRepository<Wallet> {

    List<Wallet> getAll(WalletFilterOptions walletFilterOptions);

    Wallet getByUserId(int id);
}
