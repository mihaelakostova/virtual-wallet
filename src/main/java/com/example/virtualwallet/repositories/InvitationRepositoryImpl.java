package com.example.virtualwallet.repositories;

import com.example.virtualwallet.models.Invitation;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.repositories.contracts.InvitationRepository;
import com.example.virtualwallet.repositories.contracts.TransactionRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class InvitationRepositoryImpl extends AbstractCRUDRepository<Invitation> implements InvitationRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    protected InvitationRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory, Invitation.class);
        this.sessionFactory = sessionFactory;
    }
}
