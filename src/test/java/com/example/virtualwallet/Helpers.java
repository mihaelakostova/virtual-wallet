package com.example.virtualwallet;

import com.example.virtualwallet.models.*;
import com.example.virtualwallet.models.emus.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class Helpers {

    public Helpers() {
    }

    public static User createMockAdmin() {
        User mockUser = createMockUser();
        mockUser.setRole(Role.ADMIN);
        mockUser.setBlocked(false);
        return mockUser;
    }

    public static User createMockUser() {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setName("Mock Mock");
        mockUser.setUsername("mockmock");
        mockUser.setPhone("8888888888");
        mockUser.setPassword("mockPassword1");
        mockUser.setEmail("mock@gmail.com");
        mockUser.setRole(Role.USER);
        mockUser.setBlocked(false);
        mockUser.setUserStatus(UserStatus.VERIFIED);
        mockUser.setConfirmationToken("token");


        return mockUser;
    }

    public static User createSecondMockUser() {
        var mockUser = new User();
        mockUser.setId(2);
        mockUser.setName("Mock2 Mock2");
        mockUser.setUsername("mockmock2");
        mockUser.setPhone("8888888888");
        mockUser.setPassword("mockPassword2");
        mockUser.setEmail("mock2@gmail.com");
        mockUser.setRole(Role.USER);
        mockUser.setBlocked(false);
        mockUser.setUserStatus(UserStatus.VERIFIED);


        return mockUser;
    }


    public static Wallet createMockWallet() {
        var mockWallet = new Wallet();
        mockWallet.setWallet_id(1);
        mockWallet.setNumber();
        mockWallet.setBalance(0);
        mockWallet.setCurrency(CurrencyType.BGN);
        mockWallet.setCreator(createMockUser());
        mockWallet.setBlocked(false);

        return mockWallet;
    }

    public static Wallet createSecondMockWallet() {
        var mockWallet = new Wallet();
        mockWallet.setWallet_id(2);
        mockWallet.setNumber();
        mockWallet.setBalance(0);
        mockWallet.setCurrency(CurrencyType.BGN);
        mockWallet.setCreator(createSecondMockUser());
        mockWallet.setBlocked(false);

        return mockWallet;
    }

    public static Wallet createReferMockWallet() {
        var mockWallet = new Wallet();
        mockWallet.setWallet_id(2);
        mockWallet.setNumber();
        mockWallet.setBalance(50);
        mockWallet.setCurrency(CurrencyType.BGN);
        mockWallet.setCreator(createSecondMockUser());
        mockWallet.setBlocked(false);

        return mockWallet;
    }


    public static Card createMockCard() {
        var card = new Card();
        card.setCard_id(1);
        card.setNumber("1234123412341234");
        card.setUser(createMockUser());
        card.setCheck_number("123");
        card.setMonth("01");
        card.setYear("24");
        card.setType(CardType.DEBIT);
        card.setBlocked(false);
        card.setAmount(1000);

        return card;
    }


    public static Transaction createMockTransaction(){
        Transaction transaction = new  Transaction();
        transaction.setId(1);
        transaction.setAmount(10);
        transaction.setSender(createMockWallet());
        transaction.setRecipient(createSecondMockWallet());
        transaction.setDate(LocalDateTime.now());
        transaction.setType(TransactionType.INCOMING);
        transaction.setVerificationCode("mockCode");
        transaction.setStatus(TransactionStatus.COMPLETED);
        return transaction;
    }

    public static Transfer createMockTransfer() {
        Card senderCard = new Card();
        senderCard.setUser(createMockUser());
        senderCard.setAmount(100.0);

        Wallet recipientWallet = new Wallet();
        recipientWallet.setBalance(0.0);

        Transfer transfer = new Transfer();
        transfer.setSender(senderCard);
        transfer.setRecipient(recipientWallet);
        transfer.setAmount(50.0);

        return transfer;
    }


    public static Currency createMockBGNCurrency() {
        var mockCurrency = new Currency();
        mockCurrency.setId(1);
        mockCurrency.setType(CurrencyType.BGN);

        return mockCurrency;
    }
    public static Currency createMockEURCurrency() {
        var mockCurrency = new Currency();
        mockCurrency.setId(1);
        mockCurrency.setType(CurrencyType.EUR);

        return mockCurrency;
    }

    public static Invitation createMockInvitation() {
        Invitation mockInvitation = new Invitation();
        mockInvitation.setId(1);
        mockInvitation.setInvitingUser(createMockUser());
        mockInvitation.setEmail("mock@mock.bg");
        return mockInvitation;

    }

    public static Image  createMockImage(){
        Image mockImage = new Image();
        mockImage.setId(1);
        mockImage.setBlocked(true);
        return mockImage;
    }

    public static List<Image> createImageList() {
        return List.of(new Image(), new Image(), new Image());
    }


}
