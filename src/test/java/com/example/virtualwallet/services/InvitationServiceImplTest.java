package com.example.virtualwallet.services;

import com.example.virtualwallet.Helpers;
import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.InvalidTokenException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Invitation;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.repositories.contracts.InvitationRepository;
import com.example.virtualwallet.services.InvitationServiceImpl;
import com.example.virtualwallet.services.contracts.EmailService;
import com.example.virtualwallet.services.contracts.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class InvitationServiceImplTest {

    @InjectMocks
    private InvitationServiceImpl invitationService;
    @Mock
    private InvitationRepository invitationRepository;
    @Mock
    private EmailService emailService;
    @Mock
    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateInvitation() {
        User invitingUser = Helpers.createMockUser();

        when(userService.emailExists("test@example.com")).thenReturn(false);

        invitationService.createInvitation(invitingUser, "test@example.com");

        verify(invitationRepository, times(1)).create(any(Invitation.class));
        verify(emailService, times(1)).sendReferralEmail(eq("test@example.com"), anyString(), anyString());
    }

    @Test
    void testCreateInvitationWithDuplicateEmail() {
        User invitingUser = Helpers.createMockUser();
        when(userService.emailExists("test@example.com")).thenReturn(true);

        assertThrows(DuplicateEntityException.class, () -> invitationService.createInvitation(invitingUser, "test@example.com"));

        verify(invitationRepository, never()).create(any(Invitation.class));
        verify(emailService, never()).sendReferralEmail(anyString(), anyString(), anyString());
    }

    @Test
    void testCreateInvitationWithNoReferrals() {
        User invitingUser = Helpers.createMockUser();
        invitingUser.setReferals(0);

        assertThrows(UnauthorizedOperationException.class, () -> invitationService.createInvitation(invitingUser, "test@example.com"));

        verify(invitationRepository, never()).create(any(Invitation.class));
        verify(emailService, never()).sendReferralEmail(anyString(), anyString(), anyString());
    }

    @Test
    void testValidateInvitationTokenValid() {
        Invitation invitation = Helpers.createMockInvitation();
        invitation.setExpirationDate(LocalDateTime.now().plusHours(1));
        invitation.setValid(true);

        when(invitationRepository.getByField(eq("token"), anyString())).thenReturn(invitation);

        Invitation validatedInvitation = invitationService.validateInvitationToken("validtoken");

        assertNotNull(validatedInvitation);
    }

    @Test
    void testValidateInvitationTokenInvalidToken() {
        when(invitationRepository.getByField(eq("token"), anyString())).thenReturn(null);

        assertThrows(InvalidTokenException.class, () -> invitationService.validateInvitationToken("invalidtoken"));
    }

    @Test
    void testValidateInvitationTokenExpiredToken() {
        Invitation invitation = Helpers.createMockInvitation();
        invitation.setExpirationDate(LocalDateTime.now().minusHours(1));
        invitation.setValid(true);

        when(invitationRepository.getByField(eq("token"), anyString())).thenReturn(invitation);

        assertThrows(InvalidTokenException.class, () -> invitationService.validateInvitationToken("expiredtoken"));
    }

    @Test
    void testValidateInvitationTokenUsedToken() {
        Invitation invitation = Helpers.createMockInvitation();
        invitation.setExpirationDate(LocalDateTime.now().plusHours(1));
        invitation.setValid(false);

        when(invitationRepository.getByField(eq("token"), anyString())).thenReturn(invitation);

        assertThrows(InvalidTokenException.class, () -> invitationService.validateInvitationToken("usedtoken"));
    }

    @Test
    void testGenerateToken() {
        String token = InvitationServiceImpl.generateToken();
        assertNotNull(token);
        assertEquals(6, token.length());
    }

}
