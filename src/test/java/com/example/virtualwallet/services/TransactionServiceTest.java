package com.example.virtualwallet.services;

import com.example.virtualwallet.Helpers;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.emus.UserStatus;
import com.example.virtualwallet.repositories.contracts.TransactionRepository;
import com.example.virtualwallet.services.contracts.TransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private TransactionServiceImpl transactionService;

    @Test
    public void testGetById() {
        int transactionId = 1;
        Transaction mockTransaction = Helpers.createMockTransaction();
        when(transactionRepository.getById(transactionId)).thenReturn(mockTransaction);

        Transaction result = transactionService.getById(transactionId);

        assertEquals(mockTransaction, result);
    }

    @Test
    public void testGetByCode() {
        String verificationCode = "mockCode";
        Transaction mockTransaction = Helpers.createMockTransaction();
        when(transactionRepository.getByField("verificationCode", verificationCode)).thenReturn(mockTransaction);

        Transaction result = transactionService.getByCode(verificationCode);

        assertEquals(mockTransaction, result);
    }
    @Test
    public void testGetAll(){
        when(transactionRepository.getAll())
                .thenReturn(null);

        transactionService.getAll();

        verify(transactionRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void create_Should_Call_Repository_When_AllDataIsValid(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        mockTransaction.getSender().setBalance(20);
        Set<Wallet> wallets = new HashSet<Wallet>();
        wallets.add(mockTransaction.getSender());
        mockTransaction.getSender().getCreator().setWallets(wallets);

        transactionService.create(mockTransaction, mockTransaction.getSender().getCreator());

        verify(transactionRepository, Mockito.times(1))
                .create(mockTransaction);
    }
    @Test
    public void create_Should_Throw_Exception_When_UserNotOwnerOfWallet(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        mockTransaction.getSender().setBalance(20);
        mockTransaction.getSender().setCreator(Helpers.createSecondMockUser());
        Set<Wallet> wallets = new HashSet<Wallet>();
        mockTransaction.getSender().getCreator().setWallets(wallets);

        assertThrows(UnauthorizedOperationException.class, () -> {
            transactionService.create(mockTransaction, mockTransaction.getSender().getCreator());
        });
    }

    @Test
    public void create_Should_Throw_Exception_When_UserIsBlocked(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        mockTransaction.getSender().setBalance(20);
        mockTransaction.getSender().getCreator().setBlocked(true);
        Set<Wallet> wallets = new HashSet<Wallet>();
        mockTransaction.getSender().getCreator().setWallets(wallets);

        assertThrows(UnauthorizedOperationException.class, () -> {
            transactionService.create(mockTransaction, mockTransaction.getSender().getCreator());
        });
    }
    @Test
    public void create_Should_Throw_Exception_When_UserIsNotVerified(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        mockTransaction.getSender().setBalance(20);
        mockTransaction.getSender().getCreator().setUserStatus(UserStatus.UNVERIFIED);
        Set<Wallet> wallets = new HashSet<Wallet>();
        mockTransaction.getSender().getCreator().setWallets(wallets);

        assertThrows(UnauthorizedOperationException.class, () -> {
            transactionService.create(mockTransaction, mockTransaction.getSender().getCreator());
        });
    }
    @Test
    public void create_Should_Throw_Exception_When_BothWalletsAreEqual(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        mockTransaction.getSender().setBalance(20);
        mockTransaction.setRecipient(mockTransaction.getSender());
        Set<Wallet> wallets = new HashSet<Wallet>();
        mockTransaction.getSender().getCreator().setWallets(wallets);

        assertThrows(UnauthorizedOperationException.class, () -> {
            transactionService.create(mockTransaction, mockTransaction.getSender().getCreator());
        });
    }
    @Test
    public void create_Should_Throw_Exception_When_InsufficientFunds(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        mockTransaction.getSender().setBalance(1);
        Set<Wallet> wallets = new HashSet<Wallet>();
        mockTransaction.getSender().getCreator().setWallets(wallets);

        assertThrows(UnauthorizedOperationException.class, () -> {
            transactionService.create(mockTransaction, mockTransaction.getSender().getCreator());
        });
    }
    @Test
    public void verify_Should_Throw_Exception_When_UsernamesDontMatch(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        Set<Wallet> wallets = new HashSet<Wallet>();
        mockTransaction.getSender().getCreator().setWallets(wallets);
        User newUser = Helpers.createSecondMockUser();

        assertThrows(UnauthorizedOperationException.class, () -> {
            transactionService.verify(mockTransaction, newUser, "mockCode");
        });
    }
    @Test
    public void verify_Should_Throw_Exception_When_CodesDontMatch(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        Set<Wallet> wallets = new HashSet<Wallet>();
        mockTransaction.getSender().getCreator().setWallets(wallets);

        assertThrows(UnauthorizedOperationException.class, () -> {
            transactionService.verify(mockTransaction, mockTransaction.getSender().getCreator(), "false code");
        });
    }
    @Test
    public void verify_Should_Call_Repository_When_Valid(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        Set<Wallet> wallets = new HashSet<Wallet>();
        mockTransaction.getSender().getCreator().setWallets(wallets);

        transactionService.verify(mockTransaction, mockTransaction.getSender().getCreator(), "mockCode");

        verify(transactionRepository, Mockito.times(1))
                .execute(mockTransaction);
    }
    @Test
    public void execute_Should_Throw_Exception_When_InsufficientFunds(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        mockTransaction.getSender().setBalance(1);
        Set<Wallet> wallets = new HashSet<Wallet>();
        mockTransaction.getSender().getCreator().setWallets(wallets);

        assertThrows(UnauthorizedOperationException.class, () -> {
            transactionService.execute(mockTransaction);
        });
    }
    @Test
    public void execute_Should_Call_Repository_When_Valid(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        mockTransaction.getSender().setBalance(100);
        Set<Wallet> wallets = new HashSet<Wallet>();
        mockTransaction.getSender().getCreator().setWallets(wallets);

        transactionService.execute(mockTransaction);

        verify(transactionRepository, Mockito.times(1))
                .execute(mockTransaction);
    }
    @Test
    public void cancel_Should_Call_Repository(){
        Transaction mockTransaction = Helpers.createMockTransaction();
        mockTransaction.getSender().setBalance(100);
        Set<Wallet> wallets = new HashSet<Wallet>();
        mockTransaction.getSender().getCreator().setWallets(wallets);

        transactionService.cancel(mockTransaction);

        verify(transactionRepository, Mockito.times(1))
                .delete(mockTransaction.getId());
    }
}
