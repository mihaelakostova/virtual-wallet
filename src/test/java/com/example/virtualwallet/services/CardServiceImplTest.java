package com.example.virtualwallet.services;

import com.example.virtualwallet.Helpers;
import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.repositories.contracts.CardRepository;
import com.example.virtualwallet.services.CardServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class CardServiceImplTest {

    @Mock
    private CardRepository cardRepository;

    @InjectMocks
    private CardServiceImpl cardService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetById() {
        int cardId = 1;
        Card mockCard = Helpers.createMockCard();
        when(cardRepository.getById(cardId)).thenReturn(mockCard);

        Card result = cardService.getById(cardId);

        assertEquals(mockCard, result);
    }

    @Test
    public void testGetByUser() {
        User user = Helpers.createMockUser();
        List<Card> mockCards = new ArrayList<>();
        when(cardRepository.getByUser(user)).thenReturn(mockCards);

        List<Card> result = cardService.getByUser(user);

        assertEquals(mockCards, result);
    }

    @Test
    public void testGetByNumber() {
        String cardNumber = "1234123412341234";
        Card mockCard = Helpers.createMockCard();
        when(cardRepository.getByField("number", cardNumber)).thenReturn(mockCard);

        Card result = cardService.getByNumber(cardNumber);

        assertEquals(mockCard, result);
    }

    @Test
    public void testCreateCard_Success() {
        Card card = Helpers.createMockCard();
        User user = Helpers.createMockUser();
        when(cardRepository.numberExists(card.getNumber())).thenReturn(false);

        assertDoesNotThrow(() -> cardService.create(card, user));
        verify(cardRepository, times(1)).create(card);
    }

    @Test
    public void testCreateCard_DuplicateNumber() {
        Card card = Helpers.createMockCard();
        User user = Helpers.createMockUser();
        when(cardRepository.numberExists(card.getNumber())).thenReturn(true);

        assertThrows(DuplicateEntityException.class, () -> cardService.create(card, user));
        verify(cardRepository, never()).create(card);
    }

    @Test
    public void testDeleteCard_Success() {
        int cardId = 1;
        User user = Helpers.createMockUser();
        Card mockCard = Helpers.createMockCard();
        when(cardRepository.getById(cardId)).thenReturn(mockCard);

        assertDoesNotThrow(() -> cardService.delete(cardId, user));
        verify(cardRepository, times(1)).delete(cardId);
    }

    @Test
    public void testDeleteCard_UnauthorizedOperation() {
        int cardId = 1;
        User user = Helpers.createSecondMockUser();
        Card mockCard = Helpers.createMockCard();
        when(cardRepository.getById(cardId)).thenReturn(mockCard);

        assertThrows(UnauthorizedOperationException.class, () -> cardService.delete(cardId, user));
        verify(cardRepository, never()).delete(cardId);
    }

    @Test
    public void testChangeBlockStatus_Success() {
        Card card = Helpers.createMockCard();
        User user = Helpers.createMockUser();
        when(cardRepository.getById(card.getCard_id())).thenReturn(card);

        assertDoesNotThrow(() -> cardService.changeBlockStatus(card, user));
        assertTrue(card.isBlocked());
        verify(cardRepository, times(1)).update(card);
    }

    @Test
    public void testChangeBlockStatus_UnauthorizedOperation() {
        Card card = Helpers.createMockCard();
        User user = Helpers.createSecondMockUser();
        when(cardRepository.getById(card.getCard_id())).thenReturn(card);

        assertThrows(UnauthorizedOperationException.class, () -> cardService.changeBlockStatus(card, user));
        assertFalse(card.isBlocked());
        verify(cardRepository, never()).update(card);
    }

}
