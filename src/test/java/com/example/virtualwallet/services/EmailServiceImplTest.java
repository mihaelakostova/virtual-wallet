package com.example.virtualwallet.services;

import com.example.virtualwallet.services.EmailServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class EmailServiceImplTest {
    private final String to = "recipient@example.com";

    @InjectMocks
    private EmailServiceImpl emailService;

    @Mock
    private JavaMailSender javaMailSender;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        javaMailSender = mock(JavaMailSender.class);
        emailService = new EmailServiceImpl(javaMailSender);
    }

    @Test
    void testSendEmail() {
        String to = "recipient@example.com";
        String subject = "Test Subject";
        String text = "Test Email Body";

        doNothing().when(javaMailSender).send(any(SimpleMailMessage.class));

        emailService.sendEmail(to, subject, text);

        verify(javaMailSender, times(1)).send(argThat((SimpleMailMessage mailMessage) -> {
            return mailMessage.getTo()[0].equals(to) &&
                    mailMessage.getSubject().equals(subject) &&
                    mailMessage.getText().equals(text);
        }));
    }

    @Test
    void testSendReferralEmail() {
        String username = "TestUser";
        String referralLink = "http://example.com/referral";
        emailService.sendReferralEmail(to, referralLink, username);
        verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
        ArgumentCaptor<SimpleMailMessage> mailMessageCaptor = ArgumentCaptor.forClass(SimpleMailMessage.class);
        verify(javaMailSender).send(mailMessageCaptor.capture());
        SimpleMailMessage sentMessage = mailMessageCaptor.getValue();
        assert sentMessage != null;
        assert sentMessage.getTo()[0].equals(to);
        assert sentMessage.getSubject().equals("Invitation to Join VWallet");

        String expectedText = String.format("You have been invited by %s to join our virtual wallet app! Click on the following link to register: %s", username, referralLink);
        assert sentMessage.getText().equals(expectedText);
    }

    @Test
    void testSendLargeTransactionVerificationEmail() {
        String verificationCode = "123456";
        emailService.sendLargeTransactionVerificationEmail(to, verificationCode);
        verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));

        ArgumentCaptor<SimpleMailMessage> mailMessageCaptor = ArgumentCaptor.forClass(SimpleMailMessage.class);
        verify(javaMailSender).send(mailMessageCaptor.capture());

        SimpleMailMessage sentMessage = mailMessageCaptor.getValue();
        assert sentMessage != null;
        assert sentMessage.getTo()[0].equals(to);
        assert sentMessage.getSubject().equals("Large Transaction Verification");

        String expectedText = "Please confirm the transaction by using this verification code: " + verificationCode;
        assert sentMessage.getText().equals(expectedText);
    }

    @Test
    void testSendAccountConfirmationEmail() {
        String confirmationLink = "http://example.com/confirm";

        emailService.sendAccountConfirmationEmail(to, confirmationLink);

        verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));

        ArgumentCaptor<SimpleMailMessage> mailMessageCaptor = ArgumentCaptor.forClass(SimpleMailMessage.class);
        verify(javaMailSender).send(mailMessageCaptor.capture());

        SimpleMailMessage sentMessage = mailMessageCaptor.getValue();
        assert sentMessage != null;
        assertEquals("noreply.vwallet@gmail.com", sentMessage.getFrom());
        assertEquals(to, sentMessage.getTo()[0]);
        assertEquals("Account Confirmation", sentMessage.getSubject());
        assertEquals("Thank you for registering for VWallet!\n"
                + "To activate your account, please click the link below:\n"
                + confirmationLink, sentMessage.getText());
    }


}
