package com.example.virtualwallet.services;

import com.example.virtualwallet.Helpers;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.exceptions.UnverifiedUserException;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.emus.UserStatus;
import com.example.virtualwallet.repositories.contracts.TransactionRepository;
import com.example.virtualwallet.repositories.contracts.WalletRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

public class TransactionServiceImplTest {
    @Mock
    private TransactionRepository transactionRepository;
    @Mock
    private WalletRepository walletRepository;
    @Mock
    private TransactionServiceImpl transactionService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        transactionService = new TransactionServiceImpl(transactionRepository, walletRepository);
    }



    @Test
    void verify_ValidTransaction_Success() {
        User user = Helpers.createMockUser();
        Transaction transaction = Helpers.createMockTransaction();
        when(transactionRepository.getById(anyInt())).thenReturn(transaction);

        assertDoesNotThrow(() -> transactionService.verify(transaction, user, "mockCode"));

        verify(transactionRepository, times(1)).execute(transaction);
    }

    @Test
    void verify_UnauthorizedOperationException_InvalidCode() {
        User user = Helpers.createMockUser();
        Transaction transaction = Helpers.createMockTransaction();
        when(transactionRepository.getById(anyInt())).thenReturn(transaction);

        assertThrows(UnauthorizedOperationException.class, () -> transactionService.verify(transaction, user, "invalidCode"));
    }


    @Test
    void getAllFromUser_ValidUser_Success() {
        User user = Helpers.createMockUser();
        when(transactionRepository.getAllFromUser(user)).thenReturn(List.of(Helpers.createMockTransaction()));

        List<Transaction> transactions = transactionService.getAllFromUser(user);

        assertEquals(1, transactions.size());
    }

    @Test
    void create_SenderWalletNotFound_UnauthorizedOperationException() {
        User user = Helpers.createMockUser();
        Wallet wallet1 = Helpers.createMockWallet();
        Wallet wallet2 = Helpers.createSecondMockWallet();

        if (user.getWallets() == null) {
            user.setWallets(new HashSet<>());
        }

        user.getWallets().add(wallet1);
        user.getWallets().add(wallet2);

        Transaction transaction = Helpers.createMockTransaction();
        transaction.getSender().setWallet_id(999);

        UnauthorizedOperationException exception = assertThrows(
                UnauthorizedOperationException.class,
                () -> transactionService.create(transaction, user)
        );

    }


}
