package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.InvalidRecaphaException;
import com.example.virtualwallet.models.responses.RecaptchaResponse;
import com.example.virtualwallet.services.contracts.RecaptchaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class RecaptchaServiceImplTest {

    @Mock
    private Environment environment;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private RecaptchaService recaptchaService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        recaptchaService = new RecaptchaServiceImpl(environment, restTemplate);
    }

    @Test
    public void testValidateRecaptcha_Success() {
        String recaptchaResponse = "validResponse";
        RecaptchaResponse successResponse = new RecaptchaResponse();
        successResponse.setSuccess(true);

        when(environment.getProperty("recaptcha.secret")).thenReturn("your-secret-key");
        when(restTemplate.exchange(any(String.class), any(HttpMethod.class), any(), any(Class.class)))
                .thenReturn(ResponseEntity.ok(successResponse));

        recaptchaService.validateRecaptcha(recaptchaResponse);

    }

    @Test
    public void testValidateRecaptcha_Failure() {
        String recaptchaResponse = "invalidResponse";
        RecaptchaResponse failureResponse = new RecaptchaResponse();
        failureResponse.setSuccess(false);

        when(environment.getProperty("recaptcha.secret")).thenReturn("your-secret-key");
        when(restTemplate.exchange(any(String.class), any(HttpMethod.class), any(), any(Class.class)))
                .thenReturn(ResponseEntity.ok(failureResponse));

        assertThrows(InvalidRecaphaException.class, () -> recaptchaService.validateRecaptcha(recaptchaResponse));
    }

}
