package com.example.virtualwallet.services;

import com.example.virtualwallet.Helpers;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.models.filters.WalletFilterOptions;
import com.example.virtualwallet.repositories.contracts.UserRepository;
import com.example.virtualwallet.repositories.contracts.WalletRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class WalletServiceImplTest {

    @Mock
    private WalletRepository walletRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private WalletServiceImpl walletService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        walletService = new WalletServiceImpl(walletRepository, userRepository);
    }

    @Test
    public void testGetById() {

        Wallet mockWallet = Helpers.createMockWallet();
        when(walletRepository.getById(anyInt())).thenReturn(mockWallet);

        Wallet result = walletService.getById(1);

        assertNotNull(result);
        assertEquals(mockWallet, result);
    }

    @Test
    public void testGetByNumber() {

        Wallet mockWallet = Helpers.createMockWallet();
        when(walletRepository.getByField(eq("number"), anyString())).thenReturn(mockWallet);

        Wallet result = walletService.getByNumber("123456");

        assertNotNull(result);
        assertEquals(mockWallet, result);
    }

    @Test
    public void testGetAll() {
        List<Wallet> mockWallets = new ArrayList<>();
        mockWallets.add(Helpers.createMockWallet());
        mockWallets.add(Helpers.createSecondMockWallet());
        when(walletRepository.getAll()).thenReturn(mockWallets);

        List<Wallet> result = walletService.getAll();


        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    public void testGetAllWithFilterOptions() {
        WalletFilterOptions filterOptions = new WalletFilterOptions();
        List<Wallet> mockWallets = new ArrayList<>();
        mockWallets.add(Helpers.createMockWallet());
        when(walletRepository.getAll(filterOptions)).thenReturn(mockWallets);

        List<Wallet> result = walletService.getAll(filterOptions);

        assertNotNull(result);
        assertEquals(1, result.size());
    }

    @Test
    public void testAddUserToWalletUnauthorized() {
        User loggedUser = Helpers.createMockUser();
        User user = Helpers.createSecondMockUser();
        Wallet wallet = Helpers.createSecondMockWallet();

        when(userRepository.getById(anyInt())).thenReturn(loggedUser);
        when(walletRepository.getById(anyInt())).thenReturn(wallet);

        assertThrows(UnauthorizedOperationException.class,
                () -> walletService.addUserToWallet(loggedUser, user, wallet));
    }


    @Test
    public void testRemoveUserFromWalletUnauthorized() {
        User loggedUser = Helpers.createMockUser();
        User userToRemove = Helpers.createSecondMockUser();
        Wallet wallet = Helpers.createSecondMockWallet();

        when(userRepository.getById(anyInt())).thenReturn(loggedUser);
        when(walletRepository.getById(anyInt())).thenReturn(wallet);

        assertThrows(UnauthorizedOperationException.class, () -> {
            walletService.removeUserFromWallet(loggedUser, userToRemove, wallet);
        });
    }

    @Test
    public void testChangeBlockStatus() {
        User user = Helpers.createMockUser();
        Wallet wallet = Helpers.createMockWallet();
        wallet.getHolders().add(user);

        when(walletRepository.getById(anyInt())).thenReturn(wallet);

        walletService.changeBlockStatus(wallet, user);

        assertTrue(wallet.isBlocked());
        verify(walletRepository, times(1)).update(any(Wallet.class));
    }

    @Test
    public void testUpdateWallet() {
        User user = Helpers.createMockUser();
        Wallet wallet = Helpers.createMockWallet();
        wallet.getHolders().add(user);

        when(walletRepository.getById(anyInt())).thenReturn(wallet);

        walletService.update(wallet, user);

        verify(walletRepository, times(1)).update(any(Wallet.class));
    }

    @Test
    public void testCreateWallet() {
        User user = Helpers.createMockUser();
        user.setWallets(new HashSet<>());

        when(userRepository.getById(anyInt())).thenReturn(user);
        doNothing().when(walletRepository).create(any(Wallet.class));

        walletService.create(user);

        assertEquals(1, user.getWallets().size());

        List<Wallet> walletList = new ArrayList<>(user.getWallets());
        Wallet createdWallet = walletList.get(0);

        assertFalse(createdWallet.isBlocked());
        assertNotNull(createdWallet.getNumber());
        assertEquals(0.00, createdWallet.getBalance());
        assertEquals(user, createdWallet.getCreator());
    }


    @Test
    public void testAddUserToWallet() {
        User loggedUser = Helpers.createMockUser();
        User user = Helpers.createSecondMockUser();
        Wallet wallet = Helpers.createMockWallet();
        user.setWallets(new HashSet<>());
        wallet.setCreator(loggedUser);
        wallet.getHolders().add(loggedUser);
        when(userRepository.getById(anyInt())).thenReturn(loggedUser);
        when(walletRepository.getById(anyInt())).thenReturn(wallet);

        assertDoesNotThrow(() -> walletService.addUserToWallet(loggedUser, user, wallet));

        assertTrue(wallet.getHolders().contains(user));
        assertTrue(user.getWallets().contains(wallet));
    }

    @Test
    public void testRemoveUserFromWallet() {
        User loggedUser = Helpers.createMockUser();
        User userToRemove = Helpers.createSecondMockUser();
        Wallet wallet = Helpers.createMockWallet();
        wallet.setCreator(loggedUser);

        userToRemove.setWallets(new HashSet<>());
        userToRemove.getWallets().add(wallet);
        wallet.getHolders().add(userToRemove);

        when(userRepository.getById(anyInt())).thenReturn(loggedUser);
        when(walletRepository.getById(anyInt())).thenReturn(wallet);

        assertDoesNotThrow(() -> walletService.removeUserFromWallet(loggedUser, userToRemove, wallet));

        assertFalse(wallet.getHolders().contains(userToRemove));
        assertFalse(userToRemove.getWallets().contains(wallet));
    }

    @Test
    public void testDeleteWalletUnauthorized() {
        User user = Helpers.createMockUser();
        User unauthorizedUser = Helpers.createSecondMockUser();
        Wallet wallet = Helpers.createMockWallet();
        wallet.setCreator(user);
        user.setWallets(new HashSet<>());
        unauthorizedUser.setWallets(new HashSet<>());

        when(userRepository.getById(user.getId())).thenReturn(user);
        when(userRepository.getById(unauthorizedUser.getId())).thenReturn(unauthorizedUser);

        when(walletRepository.getById(1)).thenReturn(wallet);

        try {
            walletService.delete(1, unauthorizedUser);
        } catch (UnauthorizedOperationException e) {
            verify(userRepository, never()).update(user);
            verify(walletRepository, never()).delete(1);
        }
    }

}
