package com.example.virtualwallet.services;

import com.example.virtualwallet.models.Currency;
import com.example.virtualwallet.models.ExchangeRate;
import com.example.virtualwallet.models.emus.CurrencyType;
import com.example.virtualwallet.repositories.contracts.CurrencyRepository;
import com.example.virtualwallet.repositories.contracts.ExchangeRatesRepository;
import com.example.virtualwallet.services.CurrencyExchangeServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CurrencyExchangeServiceImplTest {

    @Mock
    private ExchangeRatesRepository exchangeRatesRepository;

    @Mock
    private CurrencyRepository currencyRepository;

    @InjectMocks
    private CurrencyExchangeServiceImpl currencyExchangeService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testExchange_Success() {
        CurrencyType fromCurrencyType = CurrencyType.BGN;
        CurrencyType toCurrencyType = CurrencyType.EUR;
        double amount = 100.0;


        Currency senderCurrency = new Currency();
        senderCurrency.setId(1);
        senderCurrency.setType(fromCurrencyType);

        Currency recipientCurrency = new Currency();
        recipientCurrency.setId(2);
        recipientCurrency.setType(toCurrencyType);

        ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setFromCurrency(senderCurrency);
        exchangeRate.setToCurrency(recipientCurrency);
        exchangeRate.setRate(0.5);

        when(currencyRepository.getByField("type", fromCurrencyType)).thenReturn(senderCurrency);
        when(currencyRepository.getByField("type", toCurrencyType)).thenReturn(recipientCurrency);

        when(exchangeRatesRepository.getByType(senderCurrency, recipientCurrency)).thenReturn(exchangeRate);

        double result = currencyExchangeService.exchange(fromCurrencyType, toCurrencyType, amount);

        assertEquals(50.0, result);
    }


}
