package com.example.virtualwallet.services;

import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Card;
import com.example.virtualwallet.models.Transfer;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.contracts.CardRepository;
import com.example.virtualwallet.repositories.contracts.TransferRepository;
import com.example.virtualwallet.repositories.contracts.WalletRepository;
import com.example.virtualwallet.services.TransferServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static com.example.virtualwallet.Helpers.createMockTransfer;
import static com.example.virtualwallet.Helpers.createMockUser;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class TransferServiceImplTest {

    @Mock
    private TransferRepository transferRepository;
    @Mock
    private WalletRepository walletRepository;
    @Mock
    private CardRepository cardRepository;
    @InjectMocks
    private TransferServiceImpl transferService;
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetById() {
        int transferId = 1;
        Transfer mockTransfer = createMockTransfer();
        when(transferRepository.getById(transferId)).thenReturn(mockTransfer);

        Transfer result = transferService.getById(transferId);

        assertEquals(mockTransfer, result);
    }

    @Test
    public void testGetAll() {
        List<Transfer> mockTransfers = new ArrayList<>();
        when(transferRepository.getAll()).thenReturn(mockTransfers);

        List<Transfer> result = transferService.getAll();

        assertEquals(mockTransfers, result);
    }

    @Test
    public void testCreateTransfer_Success() {
        Transfer transfer = createMockTransfer();
        User user = createMockUser();

        doNothing().when(cardRepository).update(any(Card.class));
        doNothing().when(walletRepository).update(any(Wallet.class));

        assertDoesNotThrow(() -> transferService.create(transfer, user));
        verify(cardRepository, times(1)).update(transfer.getSender());
        verify(walletRepository, times(1)).update(transfer.getRecipient());
        verify(transferRepository, times(1)).create(transfer);
    }

    @Test
    public void testCreateTransfer_UnauthorizedOperation() {
        Transfer transfer = createMockTransfer();
        User user = createMockUser();
        user.setBlocked(true);

        assertThrows(UnauthorizedOperationException.class, () -> transferService.create(transfer, user));
        verify(cardRepository, never()).update(transfer.getSender());
        verify(walletRepository, never()).update(transfer.getRecipient());
        verify(transferRepository, never()).create(transfer);
    }

    @Test
    public void testCreateTransfer_InsufficientFunds() {
        Transfer transfer = createMockTransfer();
        User user = createMockUser();
        transfer.getSender().setAmount(0.0);

        assertThrows(UnauthorizedOperationException.class, () -> transferService.create(transfer, user));
        verify(cardRepository, never()).update(transfer.getSender());
        verify(walletRepository, never()).update(transfer.getRecipient());
        verify(transferRepository, never()).create(transfer);
    }

    @Test
    public void testWithdraw_Success() {
        Transfer transfer = createMockTransfer();
        User user = createMockUser();

        doNothing().when(walletRepository).update(any(Wallet.class));

        assertDoesNotThrow(() -> transferService.withdraw(transfer, user));
        verify(walletRepository, times(1)).update(transfer.getRecipient());
    }

    @Test
    public void testTopUp_Success() {
        Transfer transfer = createMockTransfer();
        User user = createMockUser();

        doNothing().when(walletRepository).update(any(Wallet.class));

        assertDoesNotThrow(() -> transferService.topUp(transfer, user));
        verify(walletRepository, times(1)).update(transfer.getRecipient());
    }



}
