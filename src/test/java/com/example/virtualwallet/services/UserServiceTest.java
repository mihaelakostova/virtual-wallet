package com.example.virtualwallet.services;

import com.example.virtualwallet.Helpers;
import com.example.virtualwallet.exceptions.DuplicateEntityException;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Transaction;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.models.Wallet;
import com.example.virtualwallet.repositories.contracts.TransactionRepository;
import com.example.virtualwallet.repositories.contracts.UserRepository;
import com.example.virtualwallet.services.contracts.EmailService;
import com.example.virtualwallet.services.contracts.RecaptchaService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private RecaptchaService recaptchaService;
    @Mock
    private EmailService emailService;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void emailExists_Should_Call_Repository(){
        String email = "email@email.com";
        userService.emailExists(email);

        verify(userRepository, Mockito.times(1))
                .emailExists(email);
    }
    @Test
    public void usernameExists_Should_Call_Repository(){
        String username = "kbrown";
        userService.usernameExists(username);

        verify(userRepository, Mockito.times(1))
                .usernameExists(username);
    }
    @Test
    public void getById_Should_Return_User() {
        int userId = 1;
        User mockUser = Helpers.createMockUser();
        when(userRepository.getById(userId)).thenReturn(mockUser);

        User result = userService.getById(userId);

        assertEquals(mockUser, result);
    }
    @Test
    public void getByEmail_Should_Return_User() {
        String email = "email@email.com";
        User mockUser = Helpers.createMockUser();
        when(userRepository.getByField("email", email)).thenReturn(mockUser);

        User result = userService.getByEmail(email);

        assertEquals(mockUser, result);
    }
    @Test
    public void getByUsername_Should_Return_User() {
        String username = "username";
        User mockUser = Helpers.createMockUser();
        when(userRepository.getByField("username", username)).thenReturn(mockUser);

        User result = userService.getByUsername(username);

        assertEquals(mockUser, result);
    }
    @Test
    public void getByPhone_Should_Return_User() {
        String phoneNumber = "0881234567";
        User mockUser = Helpers.createMockUser();
        when(userRepository.getByField("phone", phoneNumber)).thenReturn(mockUser);

        User result = userService.getByPhone(phoneNumber);

        assertEquals(mockUser, result);
    }
    @Test
    public void create_Should_Call_Repository_When_AllDataValid(){
        User mockUser = Helpers.createMockUser();
        String recaptcha = "recaptcha";

        when(userRepository.emailExists(mockUser.getEmail()))
                .thenReturn(false);
        when(userRepository.usernameExists(mockUser.getUsername()))
                .thenReturn(false);
        doNothing().when(recaptchaService).validateRecaptcha(recaptcha);
        doNothing().when(emailService).sendAccountConfirmationEmail(any(), any());

        // Act
        userService.create(mockUser, recaptcha);

        // Assert
        verify(recaptchaService, times(1)).validateRecaptcha(recaptcha);
        verify(userRepository, times(1)).create(mockUser);
    }
    @Test
    public void create_Should_Throw_Exception_When_UsernameNotUnique(){
        User mockUser = Helpers.createMockUser();

        String recaptcha = "recaptcha";

        doNothing().when(recaptchaService).validateRecaptcha(any());

        when(userRepository.emailExists(mockUser.getEmail()))
                .thenReturn(false);
        when(userRepository.usernameExists(mockUser.getUsername()))
                .thenReturn(true);

        assertThrows(DuplicateEntityException.class, () -> {
            userService.create(mockUser, recaptcha);
        });
    }
    @Test
    public void create_Should_Throw_Exception_When_EmailNotUnique(){
        User mockUser = Helpers.createMockUser();

        String recaptcha = "recaptcha";

        doNothing().when(recaptchaService).validateRecaptcha(any());

        when(userRepository.emailExists(mockUser.getEmail()))
                .thenReturn(true);
        when(userRepository.usernameExists(mockUser.getUsername()))
                .thenReturn(false);

        assertThrows(DuplicateEntityException.class, () -> {
            userService.create(mockUser, recaptcha);
        });
    }
    @Test
    public void verify_Should_Throw_Exception_When_TokensDontMatch(){
        String token = "token";
        when(userRepository.getByField("confirmationToken", token))
                .thenReturn(null);
        assertThrows(UnauthorizedOperationException.class, () -> {
            userService.verifyAccount(token);
        });
    }
    @Test
    public void verify_Should_Call_Repository_When_TokensMatch(){
        User user = Helpers.createMockUser();
        String token = "token";
        when(userRepository.getByField("confirmationToken", token))
                .thenReturn(user);

        userService.verifyAccount(token);

        verify(userRepository, times(1)).update(user);
    }

}
