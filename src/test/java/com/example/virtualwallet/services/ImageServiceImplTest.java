package com.example.virtualwallet.services;

import com.cloudinary.Cloudinary;
import com.cloudinary.Uploader;
import com.example.virtualwallet.Helpers;
import com.example.virtualwallet.exceptions.UnauthorizedOperationException;
import com.example.virtualwallet.models.Image;
import com.example.virtualwallet.models.User;
import com.example.virtualwallet.repositories.contracts.ImageRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.Mockito.*;


public class ImageServiceImplTest {
    @Mock
    private Cloudinary cloudinary;
    @Mock
    private Uploader uploader;
    @Mock
    private ImageRepository imageRepository;

    @InjectMocks
    private ImageServiceImpl imageService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(cloudinary.uploader()).thenReturn(uploader);
    }

    @Test
    public void testUploadImage() throws IOException {
        byte[] imageData = new byte[1];
        User user = Helpers.createMockUser();
        Map<String, Object> uploadResult = createUploadResult();

        when(cloudinary.uploader().upload(any(byte[].class), anyMap())).thenReturn(uploadResult);
        String secureUrl = imageService.uploadImage(imageData, user.getUsername());
        assertEquals("secure_url_value", secureUrl);

    }

    @Test
    public void testSave() {
        Image image = Helpers.createMockImage();
        imageService.save(image);

        verify(imageRepository, times(1)).save(image);
    }

    @Test
    public void testDelete() throws IOException {
        Image image = Helpers.createMockImage();
        imageService.delete(image.getId());

        verify(imageRepository, times(1)).delete(image.getId());
    }

    @Test
    public void testGetById() {
        Image image = Helpers.createMockImage();
        when(imageRepository.getImageById(image.getId())).thenReturn(image);

        Image checkImage = imageService.getImageById(image.getId());
        assertEquals(image, checkImage);
    }

    @Test
    public void testGetAllBlock() {
        List<Image> imageList = Helpers.createImageList();
        when(imageRepository.getAllBlock()).thenReturn(imageList);
        List<Image> checkList = imageService.getAllBlocked();

        assertEquals(imageList, checkList);
    }

    @Test
    public void testChangeBlockStatus_AdminRole() {
        Image image = Helpers.createMockImage();
        User admin = Helpers.createMockAdmin();

        when(imageRepository.getById(image.getId())).thenReturn(image);
        imageService.changeBlockStatus(image, admin);
    }


    @Test
    public void testChangeBlockStatus_UserRole() {
        Image image = Helpers.createMockImage();
        User user = Helpers.createMockUser();

        when(imageRepository.getImageById(image.getId())).thenReturn(image);
        assertThrows(UnauthorizedOperationException.class, () -> imageService.changeBlockStatus(image, user));
    }

    private Map<String, Object> createUploadResult() {
        Map<String, Object> uploadResult = mock(Map.class);
        when(uploadResult.get("secure_url")).thenReturn("secure_url_value");
        return uploadResult;
    }
}
