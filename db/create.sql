create table currencies
(
    id   int auto_increment
        primary key,
    type enum ('BGN', 'EUR', 'USD') not null
);

create table exchange_rates
(
    id               int auto_increment
        primary key,
    from_currency_id int    not null,
    to_currency_id   int    not null,
    rate             double not null,
    constraint exchange_rates_from_currencies_fk
        foreign key (from_currency_id) references currencies (id),
    constraint exchange_rates_to_currencies_fk
        foreign key (to_currency_id) references currencies (id)
);

create table images
(
    id         int auto_increment
        primary key,
    image_url  varchar(200) not null,
    is_blocked tinyint(1)   null,
    user_id    int          null,
    image_type varchar(20)  null
);

create table users
(
    user_id            int auto_increment
        primary key,
    name               varchar(40)          not null,
    username           varchar(20)          not null,
    password           varchar(200)         not null,
    email              varchar(32)          not null,
    phone              varchar(10)          not null,
    role               varchar(10)          null,
    is_admin           tinyint(1) default 0 null,
    is_blocked         tinyint(1) default 0 null,
    status             varchar(20)          not null,
    confirmation_token varchar(6)           null,
    referrals          int                  not null,
    image_id           int                  null,
    constraint fk_users_selfies
        foreign key (image_id) references images (id),
    constraint name
        check (octet_length(`name`) between 2 and 40),
    constraint username
        check (octet_length(`username`) between 2 and 20)
);

create table cards
(
    card_id      int auto_increment
        primary key,
    number       varchar(16)          not null,
    user_id      int                  not null,
    check_number int                  not null,
    year         varchar(2)           not null,
    month        varchar(2)           not null,
    type         varchar(20)          null,
    amount       int                  null,
    is_blocked   tinyint(1) default 0 not null,
    constraint number
        unique (number),
    constraint cards_users_id_fk
        foreign key (user_id) references users (user_id)
);

alter table images
    add constraint fk_images_users_id
        foreign key (user_id) references users (user_id);

create table invitations
(
    id              int auto_increment
        primary key,
    user_id         int          not null,
    email           varchar(255) not null,
    token           varchar(255) not null,
    expiration_date datetime     not null,
    is_valid        tinyint(1)   null,
    constraint invitations_ibfk_1
        foreign key (user_id) references users (user_id)
);

create index user_id
    on invitations (user_id);

create table wallets
(
    wallet_id  int auto_increment
        primary key,
    number     varchar(16)              not null,
    user_id    int                      not null,
    balance    DECIMAL(10, 2) DEFAULT 0.00 not null,
    is_blocked tinyint(1) default 0     not null,
    is_deleted tinyint(1) default 0     not null,
    currency   varchar(3) default 'BGN' null,
    constraint number
        unique (number),
    constraint wallets_users_id_fk
        foreign key (user_id) references users (user_id)
);

create table transactions
(
    transaction_id    int auto_increment
        primary key,
    amount            int                                   not null,
    sender_id         int                                   not null,
    recipient_id      int                                   not null,
    transaction_date  timestamp default current_timestamp() not null,
    type              varchar(20)                           not null,
    verification_code varchar(10)                           null,
    status            varchar(20)                           not null,
    constraint transactions_ib_fk_1
        foreign key (sender_id) references wallets (wallet_id),
    constraint transactions_ib_fk_2
        foreign key (recipient_id) references wallets (wallet_id)
);

create index recipient_id
    on transactions (recipient_id);

create index sender_id
    on transactions (sender_id);

create table transfers
(
    transfer_id      int auto_increment
        primary key,
    amount           int                                   not null,
    sender_id        int                                   not null,
    recipient_id     int                                   not null,
    transaction_date timestamp default current_timestamp() not null,
    constraint transfers_ib_fk_1
        foreign key (sender_id) references cards (card_id),
    constraint transfers_ib_fk_2
        foreign key (recipient_id) references wallets (wallet_id)
);

create table users_wallets
(
    user_id   int not null,
    wallet_id int not null,
    constraint users_wallets_user_id_fk
        foreign key (user_id) references users (user_id),
    constraint users_wallets_wallets_id_fk
        foreign key (wallet_id) references wallets (wallet_id)
);

