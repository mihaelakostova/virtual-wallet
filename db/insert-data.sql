insert into users (name, username, password, email, phone, role, is_admin, is_blocked, status, confirmation_token, referrals)
values
    ('Mary Gibson', 'mgibson', 'rfv567RD+S', 'mary_gibson@gmail.com', '0896543208', 'USER',  false, true,  'VERIFIED', null, 10),
    ('Frank Baker', 'fbaker', 'yhn759TG*V', 'frank_baker@gmail.com', '0896543421', 'USER',  false, true,  'VERIFIED', null, 10),
    ('David Bradley', 'dbradley', '123ujmYU+I', 'david_bradley@gmail.com', '0896543478', 'ADMIN', true, false, 'VERIFIED', null, 10),
    ('Kevin Brown', 'kbrown', 'ght567RF*V', 'kevin_brown@gmail.com', '0896543465', 'ADMIN', true, false, 'VERIFIED', null, 10),
    ('Barry Johnson', 'bjohnson', 'HNJ789rd+f', 'barry_johnson@gmail.com', '0896543498', 'ADMIN',  true, false,  'VERIFIED', null, 10),
    ('Steven Campbell', 'scampbell', 'the456RF&^V', 'steven_campbell@gmail.com', '0896543443', 'ADMIN', true, false,  'VERIFIED', null, 10),
    ('Simon Russell', 'srussel', 'JIUrfv97&4', 'simon_russell@gmail.com', '0896543402', 'USER',  false, false,  'VERIFIED', null, 10),
    ('George Evans', 'gevans', '456GVFkn+t', 'george_evans@gmail.com', '0896543437', 'ADMIN',  false, false,  'VERIFIED', null, 10),
    ('Laura Hamilton', 'lhamilton', '537yhfTG&V', 'laura_hamilton@gmail.com', '0896543543', 'USER',  false, false,  'VERIFIED', null, 10),
#     ('Mary Gibson', 'mgibson', 'rfv567RD+S', 'mary_gibson@gmail.com', '0896543208', 'USER',  false, false,  'VERIFIED', null, 10),
#     ('Frank Baker', 'fbaker', 'yhn759TG*V', 'frank_baker@gmail.com', '0896543421', 'USER',  false, false,  'VERIFIED', null, 10),
    ('Georgi Dimitrov', 'georgi', 'A@password1', 'georgi@gmail.com', '0896543419', 'USER',  false, false,  'VERIFIED', null, 10),
    ('John Smith', 'jsmith', 'smithpa+ss', 'john_smith@gmail.com', '0896543601', 'USER', false, false,  'VERIFIED', null, 10),
    ('Michael Johnson', 'mjohnson', 'johnson&123', 'michael_johnson@gmail.com', '0896543620', 'USER', false, false,  'VERIFIED', null, 10),
    ('Sarah Williams', 'swilliams', 'williams+456', 'sarah_williams@gmail.com', '0896543644', 'USER', false, false,  'VERIFIED', null, 10),
    ('Emily Taylor', 'etaylor', 'taylor&pass', 'emily_taylor@gmail.com', '0896543665', 'USER', false, false,  'VERIFIED', null, 10),
    ('Daniel Martinez', 'dmartinez', 'martinez&123', 'daniel_martinez@gmail.com', '0896543688', 'USER', false, false,  'VERIFIED', null, 10),
    ('Jessica Davis', 'jdavis', 'davis&pass', 'jessica_davis@gmail.com', '0896543702', 'USER', false, false,  'VERIFIED', null, 10),
    ('James Anderson', 'janderson', 'anderson&123', 'james_anderson@gmail.com', '0896543724', 'USER', false, false,  'VERIFIED', null, 10),
    ('Megan Wilson', 'mwilson', 'wilson&pass', 'megan_wilson@gmail.com', '0896543745', 'USER', false, false,  'VERIFIED', null, 10),
    ('Robert Thomas', 'rthomas', 'thomas&123', 'robert_thomas@gmail.com', '0896543767', 'USER', false, false,  'VERIFIED', null, 10),
    ('Amanda Clark', 'aclark', 'clark&pass', 'amanda_clark@gmail.com', '0896543788', 'USER', false, false,  'VERIFIED', null, 10),
    ('Christopher Taylor', 'ctaylor', 'taylor&pass', 'christopher_taylor@gmail.com', '0896543801', 'USER', false, false,  'VERIFIED', null, 10),
    ('Stephanie Martinez', 'smartinez', 'martinez&123', 'stephanie_martinez@gmail.com', '0896543820', 'USER', false, false,  'VERIFIED', null, 10),
    ('Joseph White', 'jwhite', 'white&pass', 'joseph_white@gmail.com', '0896543844', 'USER', false, false,  'VERIFIED', null, 10),
    ('Samantha Allen', 'sallen', 'allen&123', 'samantha_allen@gmail.com', '0896543865', 'USER', false, false,  'VERIFIED', null, 10),
    ('William Scott', 'wscott', 'scott&pass', 'william_scott@gmail.com', '0896543888', 'USER', false, false,  'VERIFIED', null, 10),
    ('Olivia Adams', 'oadams', 'adamspass', 'olivia_adams@gmail.com', '0896543902', 'USER', false, false,  'VERIFIED', null, 10),
    ('David Garcia', 'dgarcia', 'garcia&pass', 'david_garcia@gmail.com', '0896543924', 'USER', false, false,  'VERIFIED', null, 10),
    ('Jennifer Hernandez', 'jhernandez', 'hernandez&123', 'jennifer_hernandez@gmail.com', '0896543945', 'USER', true, false,  'VERIFIED', null, 10),
    ('Charles Hall', 'chall', 'hall&pass', 'charles_hall@gmail.com', '0896543967', 'USER', false, false,  'VERIFIED', null, 10),
    ('Natalie Young', 'nyoung', 'young&123', 'natalie_young@gmail.com', '0896543988', 'USER', false, false,  'VERIFIED', null, 10);


insert into wallets (number, user_id, balance, is_deleted, is_blocked,currency)
VALUES ('1234567890123456', 1, 200.00,0,0,'BGN'),
       ('2345678901234567', 2, 150.00,0,0,'BGN'),
       ('3456789012345678', 3, 300.00,0,0,'BGN'),
       ('4567890123456789', 4, 75.00,0,0,'BGN'),
       ('5678901234567890', 10, 500.00,0,0,'BGN');


insert into users_wallets (user_id, wallet_id)
values (1,1),
       (2,2),
       (3,3),
       (4,4),
       (10,5);


INSERT INTO cards (number, user_id, check_number,year,month, type, amount,is_blocked) VALUES
                                                                                     ('1234567890123456', 4, 234, 24,09, 'DEBIT', 5000,0),
                                                                                     ('9876543210987654', 1, 678, 25,07, 'DEBIT', 7500,0),
                                                                                     ('1111222233334444', 2, 321,25,01 , 'DEBIT', 3000,0),
                                                                                     ('5555666677778888', 3, 765, 24,10, 'DEBIT', 10000,0),
                                                                                     ('9999000011112222', 10, 876, 24,09, 'DEBIT', 2000,0);
INSERT INTO transactions (amount, sender_id, recipient_id, type, status, verification_code)
VALUES
(75, 4, 3, 'OUTGOING', 'COMPLETED', '98765'),
(30, 5, 2, 'OUTGOING', 'PENDING', '23456'),
(60, 1, 3, 'OUTGOING', 'COMPLETED', '87654'),
(20, 3, 2, 'OUTGOING', 'PENDING', '34567'),
(45, 2, 4, 'OUTGOING', 'PENDING', '78901'),
(15, 5, 1, 'OUTGOING', 'COMPLETED', '45678'),
(1000, 4, 3, 'OUTGOING', 'COMPLETED', '98766'),
(30, 5, 2, 'OUTGOING', 'PENDING', '23455'),
(300, 1, 3, 'OUTGOING', 'COMPLETED', '87655'),
(120, 3, 2, 'OUTGOING', 'PENDING', '34566'),
(376, 2, 4, 'OUTGOING', 'PENDING', '78902'),
(15, 5, 1, 'OUTGOING', 'COMPLETED', '45677'),
(75, 4, 3, 'OUTGOING', 'COMPLETED', '98767'),
(30, 5, 2, 'OUTGOING', 'PENDING', '23454'),
(60, 1, 3, 'OUTGOING', 'COMPLETED', '87656'),
(20, 3, 2, 'OUTGOING', 'PENDING', '34565'),
(45, 2, 4, 'OUTGOING', 'PENDING', '78903'),
(15, 5, 1, 'OUTGOING', 'COMPLETED', '45676'),
(75, 4, 3, 'OUTGOING', 'COMPLETED', '98768'),
(30, 5, 2, 'OUTGOING', 'PENDING', '23453'),
(190, 1, 3, 'OUTGOING', 'COMPLETED', '87657'),
(20, 3, 2, 'OUTGOING', 'PENDING', '34564'),
(45, 2, 4, 'OUTGOING', 'PENDING', '78904'),
(15, 5, 1, 'OUTGOING', 'COMPLETED', '45675'),
(75, 4, 3, 'OUTGOING', 'COMPLETED', '98769'),
(30, 5, 2, 'OUTGOING', 'PENDING', '23452'),
(60, 1, 3, 'OUTGOING', 'COMPLETED', '87658'),
(20, 3, 2, 'OUTGOING', 'PENDING', '34563'),
(1200, 2, 4, 'OUTGOING', 'PENDING', '78905'),
(15, 5, 1, 'OUTGOING', 'COMPLETED', '45674'),
(75, 4, 3, 'OUTGOING', 'COMPLETED', '98761'),
(30, 5, 2, 'OUTGOING', 'PENDING', '23451'),
(60, 1, 3, 'OUTGOING', 'COMPLETED', '87659'),
(20, 3, 2, 'OUTGOING', 'PENDING', '34562'),
(45, 2, 4, 'OUTGOING', 'PENDING', '78906'),
(15, 5, 1, 'OUTGOING', 'COMPLETED', '45673');

INSERT INTO currencies(type)
VALUES ('BGN'),
       ('EUR'),
       ('USD');

INSERT INTO exchange_rates(from_currency_id, to_currency_id, rate)
VALUES (1, 2, 0.51),
       (1, 3, 0.55),
       (2, 1, 1.96),
       (2, 3, 1.08),
       (3, 1, 1.81),
       (3, 2, 0.93);
